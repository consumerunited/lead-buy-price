import os
import sys
import json as jsn

from analytics.restplus import analytics

from flask import request, jsonify
from flask_restplus import Resource
from ..model_data import ns

from analytics.serializers.support_data_values import serialized_support_data_value_up, serialized_support_data_value_return, serialized_all_support_data_value_return, serialized_support_data_value
from db_model.db import db
from db_model.support_data_values import support_data_values


@ns.route('/suppport_data/<int:support_data_id>/value/')
class create_support_data_value(Resource):

    @analytics.marshal_with(serialized_all_support_data_value_return, code = 200)
    def get(self,support_data_id):
        """ 
        Returns all support data types
        """
        support_data_value_list = []
        for p in db.session.query(support_data_values).filter(support_data_values.support_data_id == support_data_id).all():
            support_data_value_list.append({'support_data_id':p.support_data_id,'support_data_value_id':p.support_data_value_id,'key':p.key,'value':p.value})

        support_data_values_dict = {'support_data_values':support_data_value_list}
        
        return support_data_values_dict, 200


    @analytics.expect(serialized_support_data_value)
    @analytics.response(201, 'Support data value successfully created')
    @analytics.response(409, 'Support data value already exists')
    def post(self, support_data_id):
        """
        Creates a new type of support data
        """
        data = request.json

        # Parse out data
        key = data['key']
        value = data['value']

        # Does the parameter exist?
        #parameter_current_state =  db.session.query(parameters).filter(parameters.parameter_name == str(parameter_name)).first()
        support_data_current_state =  db.session.query(support_data_values).filter((support_data_values.support_data_id == support_data_id) & (support_data_values.key == key)).first()
        
        # If it does not create new parameter
        if support_data_current_state == None:
            newSupportDataValue = support_data_values(support_data_id=support_data_id, key=key, value=value)
            db.session.add(newSupportDataValue)
            db.session.commit()

            return None, 201

        # If it does exist return with already exists
        else:

            return None, 409
