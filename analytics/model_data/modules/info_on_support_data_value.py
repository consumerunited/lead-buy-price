
import os
import sys
import json as jsn

from analytics.restplus import analytics

from flask import request, jsonify
from flask_restplus import Resource
from ..model_data import ns

from analytics.serializers.support_data_values import serialized_support_data_value_up, serialized_support_data_value_return, serialized_all_support_data_value_return, serialized_support_data_value
from db_model.db import db
from db_model.support_data_values import support_data_values

@ns.route('/suppport_data/<int:support_data_id>/value/<int:support_data_value_id>')
class info_on_support_data_Value(Resource):

    @analytics.marshal_with(serialized_support_data_value_return, code = 200)
    def get(self, support_data_id, support_data_value_id):
        """ 
        Returns a support data value
        """
        p = db.session.query(support_data_values).filter((support_data_values.support_data_id == support_data_id) & (support_data_values.support_data_value_id == support_data_value_id)).first()

        if p != None:
            parmeter_dict = {'support_data_id':p.support_data_id,'support_data_value_id':p.support_data_value_id,'key':p.key,'value':p.value}

            return parmeter_dict, 201

        else:
            return None, 404

    @analytics.expect(serialized_support_data_value_up)
    @analytics.response(204, 'Parameter successfully updated')
    @analytics.response(404, 'Parameter not found')
    def put(self, support_data_id, support_data_value_id):
        """
        Updates a support data value
        """
        p = db.session.query(support_data_values).filter((support_data_values.support_data_id == support_data_id) & (support_data_values.support_data_value_id == support_data_value_id)).first()

        if p != None:

            data = request.json

            # Parse out data
            value = data['value']

            # Update and commit
            p.value = value
            db.session.commit()

            return None, 204
        else:
            return None, 404

    @analytics.response(204, 'Parameter successfully deleted')
    @analytics.response(404, 'Parameter not found')
    def delete(self, support_data_id, support_data_value_id):
        """
        Deletes a support data value
        """
        p = db.session.query(support_data_values).filter((support_data_values.support_data_id == support_data_id) & (support_data_values.support_data_value_id == support_data_value_id)).first()

        if p != None:
            db.session.delete(p)
            db.session.commit()
            return None, 204
        else:
            return None, 404