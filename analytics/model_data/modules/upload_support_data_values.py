import os
import sys
import json as jsn

from analytics.restplus import analytics

from flask import request, jsonify
from flask_restplus import Resource
from ..model_data import ns

from analytics.serializers.support_data_values import serialized_support_data_value_up, serialized_support_data_value_return, serialized_all_support_data_value_return, serialized_support_data_value
from db_model.db import db
from db_model.support_data_values import support_data_values
from werkzeug.datastructures import FileStorage

import csv
from db_model.support_data_values import support_data_values

upload_parser = analytics.parser()
upload_parser.add_argument('file', location='files',type=FileStorage, required=True)


# Allowed file types for this method
ALLOWED_EXTENSIONS = set(['csv'])
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@ns.route('/suppport_data/<int:support_data_id>/upload/', methods = ['POST'])

@ns.expect(upload_parser)
class upload_support_data_values(Resource):

    def post(self,support_data_id):
    	"""
    	Upload a CSV of support data values
    	"""

    	# Get and save the file
        uploaded_file = request.files['file']

        if uploaded_file and allowed_file(uploaded_file.filename):
        	output_file_path = os.environ['APP_ROOT'] + "/analytics/model_data/uploads/temp_upload.csv"

        	# Clean up file name and save
	        uploaded_file.save(output_file_path)

	        try:
		        # Read the File and validate
		        with open(output_file_path,'rU') as f:
		        	data = csv.reader(f)
		        	count = 0
		        	for row in data:
		        		if count != 0:
		        			# Does this exist?
		        			sv = db.session.query(support_data_values).filter((support_data_values.support_data_id == support_data_id) & (support_data_values.key == row[0])).first()
		        			if sv == None:
			        			newSupportDataValue = support_data_values(support_data_id=support_data_id, key=row[0], value=row[1])
			        			db.session.add(newSupportDataValue)
			        		else:
			        			sv.value=row[1]
		        		count += 1
		        db.session.commit()
		       
		        os.remove(output_file_path)

		        message = 'File uploaded successfully'
		        return message, 201    
	        except Exception as e:
	        	os.remove(output_file_path)

		    	return str(e), 400
        else:
        	return 'Not supported file type, must be csv', 400

