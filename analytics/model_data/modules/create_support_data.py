import os
import sys
import json as jsn

from analytics.restplus import analytics

from flask import request, jsonify
from flask_restplus import Resource
from ..model_data import ns

from analytics.serializers.support_data import serialized_support_data, serialized_support_data_return, serialized_all_support_data
from db_model.db import db
from db_model.support_data import support_data


@ns.route('/suppport_data/')
class create_support_data(Resource):

    @analytics.marshal_with(serialized_all_support_data, code = 200)
    def get(self):
        """ 
        Returns all support data for a model
        """
        support_data_list = []
        for p in db.session.query(support_data).all():
            support_data_list.append({'support_data_id':p.support_data_id,'name':p.name, 'description':p.description})

        support_data_dict = {'support_data':support_data_list}

        return support_data_dict, 200


    @analytics.expect(serialized_support_data)
    @analytics.response(201, 'Support data successfully created')
    @analytics.response(409, 'Support data already exists')
    def post(self):
        """
        Creates a new type of support data for a model
        """
        data = request.json

        # Parse out data
        name = data['name']
        description = data['description']

        # Does the parameter exist?
        #parameter_current_state =  db.session.query(parameters).filter(parameters.parameter_name == str(parameter_name)).first()
        parameter_current_state =  db.session.query(support_data).filter(support_data.name == str(name)).first()
        
        # If it does not create new parameter
        if parameter_current_state == None:
            newSupportData = support_data(name=name, description=description)
            db.session.add(newSupportData)
            db.session.commit()

            return None, 201

        # If it does exist return with already exists
        else:

            return None, 409
