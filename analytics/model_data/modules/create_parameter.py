
import os
import sys
import json as jsn

from analytics.restplus import analytics

from flask import request, jsonify
from flask_restplus import Resource
from ..model_data import ns

from analytics.serializers.model_parameter import serialized_parameter, serialized_parameter_return, serialized_all_parameter_return, serialized_parameter_value
from db_model.db import db
from db_model.model_parameters import model_parameters

@ns.route('/<int:model_id>/parameter/')
class create_parameters(Resource):

    @analytics.marshal_with(serialized_all_parameter_return, code = 200)
    def get(self,model_id):
        """ 
        Returns all parameters for a model
        """
        parameter_list = []
        for p in db.session.query(model_parameters).filter(model_parameters.model_id == model_id).all():
            parameter_list.append({'model_id':p.model_id,'parameter_id':p.parameter_id,'name':p.name, 'value':p.value,'description':p.description})

        parmeter_dict = {'parameters':parameter_list}

        return parmeter_dict, 200



    @analytics.expect(serialized_parameter)
    @analytics.response(201, 'Parameter successfully created')
    @analytics.response(409, 'Parameter already exists')
    def post(self, model_id):
        """
        Creates a new parameter 
        """
        data = request.json

        # Parse out data
        model_id = model_id
        name = data['name']
        value = data['value']
        description = data['description']

        # Does the parameter exist?
        #parameter_current_state =  db.session.query(parameters).filter(parameters.parameter_name == str(parameter_name)).first()
        parameter_current_state =  db.session.query(model_parameters).filter(model_parameters.name == str(name)).first()
        
        # If it does not create new parameter
        if parameter_current_state == None:
            newParameter = model_parameters(model_id=model_id, name=name, value=value, description=description)
            db.session.add(newParameter)
            db.session.commit()

            return None, 201

        # If it does exist return with already exists
        else:

            return None, 409






