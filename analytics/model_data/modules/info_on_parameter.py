
import os
import sys
import json as jsn

from analytics.restplus import analytics

from flask import request, jsonify
from flask_restplus import Resource
from ..model_data import ns

from analytics.serializers.model_parameter import serialized_parameter, serialized_parameter_return, serialized_all_parameter_return, serialized_parameter_value
from db_model.db import db
from db_model.model_parameters import model_parameters

@ns.route('/<int:model_id>/parameter/<int:parameter_id>')
class info_on_parameter(Resource):

    @analytics.marshal_with(serialized_parameter_return, code = 200)
    def get(self, model_id, parameter_id):
        """ 
        Returns a parameter
        """
        p = db.session.query(model_parameters).filter((model_parameters.model_id == model_id) & (model_parameters.parameter_id == parameter_id)).first()

        if p != None:
            parmeter_dict = {'model_id': p.model_id,'parameter_id':p.parameter_id,'name':p.name, 'value':p.value,'description':p.description}

            return parmeter_dict, 201

        else:
            return None, 404

    @analytics.expect(serialized_parameter_value)
    @analytics.response(204, 'Parameter successfully updated')
    @analytics.response(404, 'Parameter not found')
    def put(self, model_id, parameter_id):
        """
        Updates a parameter
        """
        # Get the parameter
        p = db.session.query(model_parameters).filter((model_parameters.model_id == model_id) & (model_parameters.parameter_id == parameter_id)).first()

        if p != None:

            data = request.json

            # Parse out data
            parameter_value = data['value']

            # Update and commit
            p.value = parameter_value
            db.session.commit()

            return None, 204
        else:
            return None, 404

    @analytics.response(204, 'Parameter successfully deleted')
    @analytics.response(404, 'Parameter not found')
    def delete(self, model_id, parameter_id):
        """
        Deletes a parameter
        """
        p = db.session.query(model_parameters).filter((model_parameters.model_id == model_id) & (model_parameters.parameter_id == parameter_id)).first()

        if p != None:
            db.session.delete(p)
            db.session.commit()
            return None, 204
        else:
            return None, 404