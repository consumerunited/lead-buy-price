import os
import sys
import json as jsn

from analytics.restplus import analytics

from flask import request, jsonify
from flask_restplus import Resource
from ..model_data import ns

from analytics.serializers.model import serialized_model, serialized_all_model_return
from db_model.db import db
from db_model.model import model


@ns.route('/')
class create_model(Resource):

    @analytics.marshal_with(serialized_all_model_return, code = 200)
    def get(self):
        """ 
        Returns all models
        """
        model_list = []
        for p in db.session.query(model).all():
            model_list.append({'id':p.model_id,'name':p.name, 'description':p.description})

        model_dict = {'models':model_list}

        return model_dict, 200

    @analytics.expect(serialized_model)
    @analytics.response(201, 'Model successfully created')
    @analytics.response(409, 'Model already exists')
    def post(self):
        """
        Creates a new model 
        """
        data = request.json

        # Parse out data
        name = data['name']
        description = data['description']

        # Does the model exist?
        model_current_state =  db.session.query(model).filter(model.name == str(name)).first()
        
        # If it does not create new model
        if model_current_state == None:
            newModel = model(name=name, description=description)
            db.session.add(newModel)
            db.session.commit()

            return None, 201

        # If it does exist return with already exists
        else:

            return None, 409
            