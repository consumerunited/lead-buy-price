# Configuration for person
from flask_restplus import fields
from analytics.restplus import analytics

from phone import phone
from address import address
from identified_value import identified_value
from audit_info import audit_info

license_status_enum = ['VALID','CANADIAN','FOREIGN','PERMIT','EXPIRED','SUSPENDED','CANCELLED','UNLICENSED','REVOKED']
education_enum = ['NONE','GED','HIGH_SCHOOL','SOME_COLLGE','ASSOCIATES','IN_COLLEGE','BACHELORS','VOCATIONAL','MASTERS','DOCTORATE','MEDICAL','LAW','COMM_COLL','UNKNOWN']
marital_status_enum = ['DIVORCED','PARTNERED','MARRIED','SEPARATED','SINGLE','WIDOWED','DOM_PARTNER']
credit_enum = ['EXCELLENT','VERY_GOOD','GOOD', 'FAIR','BELOW_FAIR','POOR']
home_ownership_enum = ['APARTMENT','CONDO_OWNED','HOME_OWNED','RENTAL','WITH_PARENT','MOBILE_HOME','OTHER']
gender_enum = ['M','F','O']

person = 	analytics.model('person', {
	'id': fields.Integer(required=True, description = 'ID for the person'),
	'first_name': fields.String(max_length=50, description = 'First name of the person'),
	'last_name': fields.String(max_length=50, description = 'Last name of the person'),
	'middle_name': fields.String(max_length=50, description = 'Middle name of the person'),
	'date_of_birth': fields.Date(description = 'Date of birth of the person'),
	'license_number': fields.String(description = 'License number of the person'),
	'license_state': fields.String(description = 'State of the license'),
	'license_expiration': fields.Date(description = 'Exipiration date of the license'),
	'date_first_licensed': fields.Date(description = 'Date first licensed'),
	'license_status': fields.String(enum= license_status_enum, description = 'Status of the persons license'),
	'phones': fields.List(fields.Nested(phone), description = 'List of phone numbers for the person'),
	'addresses': fields.List(fields.Nested(address), description = 'List of addresses for the person'),
	'email': fields.String(max_length = 255, description = 'Email address of the person'),
	'education': fields.String(enum = education_enum, description = 'Highest level of education'),
	'marital_status': fields.String(enum= marital_status_enum, description = 'Marital status'),
	'credit': fields.String(enum = credit_enum, description = 'Credit level'),
	'home_ownership': fields.String(enum = home_ownership_enum, description = 'Homeownership'),
	'occupation': fields.Nested(identified_value, description = 'Occupaiont of the person'),
	'created': fields.Nested(audit_info),
	'gender': fields.String(enum = gender_enum, description = 'Gender of the person')
	})
