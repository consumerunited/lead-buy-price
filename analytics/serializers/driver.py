# Configuration for driver
from flask_restplus import fields
from analytics.restplus import analytics

from person import person

rating_enum = ['RATED','NOT_RATED','EXCLUDED','UNLICENSED','DEFERRED','OTHER']
relation_enum = ['INSURED','SPOUSE','PARTNER','PARENT','SIBLING','CHILD','RELATIVE','ROOMMATE','OTHER']

driver = analytics.inherit('driver', person, {
	'rating': fields.String(enum = rating_enum, description = 'How is this driver rated'),
	'relation': fields.String(enum = relation_enum, description = 'How is this driver related to the containing entity')
	})
