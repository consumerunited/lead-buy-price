# Configuration for Customer Data
from flask_restplus import fields
from analytics.restplus import analytics

from identified_value import identified_value
from coded_value import coded_value
from person import person 
from residence import residence
from coverage import coverage
from answer import answer
from tier_info import tier_info

sms_opt_in_enum = ["ALLOWED",'BLOCKED']

customer = analytics.model('customer', {
	'contact1': fields.Nested(person),
	'contact2': fields.Nested(person),
	'vehicles': fields.List(fields.Nested(identified_value), description = 'The list of vehicles that may be covered by a policy held by this customer'),
	'residence_info': fields.Nested(residence, descripition="Residence info for home policies"),
	'policies': fields.List(fields.Nested(identified_value), descripition = 'The list of policies held by this customer'),
	#'external_ids': fields.Nested({}),
	'originating_lead': fields.List(fields.Nested(identified_value), descripition = 'The lead this customer originated from'),
	'credited_partner': fields.List(fields.Nested(identified_value), descripition = 'The marketing partner credited with the acquisition of this customer'),
	'credited_agent': fields.List(fields.Nested(identified_value), descripition = 'The marketing partner credited with the acquisition of this customer'),
	'claims': fields.List(fields.Nested(identified_value), descripition = 'The list of claims that are associated to this customer'),
	'violations': fields.List(fields.Nested(identified_value), descripition = 'The list of violations recorded on this customer'),
	'credited_team': fields.List(fields.Nested(identified_value), descripition = 'The employee team credited with the acquisition of this customer'),
	'credited_campaign': fields.List(fields.Nested(identified_value), descripition = 'The marketing campaign credited with the acquisition of this customer'),
	'coverages': fields.List(fields.Nested(coverage), descripition = 'A list of coverages requested by this customer (i.e. all vehicles)'),
	'disposition': fields.Nested(coded_value, descripition = 'The reason for the current or updated state of the customer'),
	'sms_opt_in': fields.String(enum=sms_opt_in_enum, descripition = 'What is the customers SMS messaging preferences?'),
	'answers': fields.List(fields.Nested(answer), description = 'A list of answers to questions applicable for this entire customer (i.e. all vehicles and drivers or unrelated to either). Answers specific to drivers or vehicles are returned from the associated endpoints'),
	'tier_info': fields.Nested(tier_info, descripition = "Customer tier information related to score of customer")
	})