# Configuration for Lead
from flask_restplus import fields
from analytics.restplus import analytics

# What we expect the input to look like
serialized_support_data = 	analytics.model('support_data', {
	'name': fields.String(required=True, description = 'Name of the support data'),
	'description': fields.String(description = 'Description of how the support data is to be used')
	})

# How we return a single parameter on a get
serialized_support_data_return = 	analytics.model('support_data_return', {
	'support_data_id': fields.Integer(required=True, description = 'ID of the support data'),
	'name': fields.String(required=True, description = 'Name of the parameter'),
	'description': fields.String(description = 'Description of how the parameter is to be used')
	})

# Return all parameters
serialized_all_support_data = 	analytics.model('all_support_data_return', {
	'support_data': fields.List(fields.Nested(serialized_support_data_return), description = 'List of support data')
	})
