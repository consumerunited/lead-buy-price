# Configuration for address info
from flask_restplus import fields
from analytics.restplus import analytics

address = 	analytics.model('address', {
	'id': fields.Integer(),
	'street1': fields.String(max_length = 255),
	'street2': fields.String(max_length = 255),
	'city': fields.String(max_length = 50),
	'state': fields.String(max_length = 2),
	'county': fields.String(),
	'zip_code': fields.String(max_length =10, min_length = 5)
	})
