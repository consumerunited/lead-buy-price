# Configuration for prior policy info
from flask_restplus import fields
from analytics.restplus import analytics

from identified_value import identified_value
from coded_value import coded_value

liability_limit_enum = ['NONE','ST_MINIMUM','IL25_50','IL50_100','IL100_100','IL100_300','IL250_500','IL300_300','IL500_500','IL1000_1000','CSL55','CSL100','CSL300','CSL500']

prior_policy = 	analytics.model('prior_policy', {
	'customer': fields.Nested(identified_value),
	'expiration_date': fields.Date(),
	'carrier': fields.Nested(coded_value),
	'term': fields.Integer(),
	'premium': fields.Float(),
	'liability_limit': fields.String(enum = liability_limit_enum, description = 'The prior liability limits the custome had with their carrier'),
	'months_with_prior_carrier': fields.Integer(),
	'months_with_continuous_coverage': fields.Integer(),
	'as_of_date': fields.Date(),
	'home_months_with_prior_carrier': fields.Integer(),
	'home_months_with_continuous_coverage': fields.Integer(),
	'home_expiration_date': fields.Date(),
	'home_carrier': fields.Nested(coded_value)
	})
