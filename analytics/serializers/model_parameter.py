# Configuration for Lead
from flask_restplus import fields
from analytics.restplus import analytics

# What we expect the input to look like
serialized_parameter = 	analytics.model('parameter', {
	'name': fields.String(required=True, description = 'Name of the parameter'),
	'value': fields.String(required=True, description = 'Value of the parameter'),
	'description': fields.String(description = 'Description of how the parameter is to be used')
	})

# How we return a single parameter on a get
serialized_parameter_return = 	analytics.model('parameter_return', {
	'model_id': fields.Integer(required=True, description = 'ID of the model'),
	'parameter_id': fields.Integer(required=True, description = 'ID of the parameter'),
	'name': fields.String(required=True, description = 'Name of the parameter'),
	'value': fields.String(required=True, description = 'Value of the parameter'),
	'description': fields.String(description = 'Description of how the parameter is to be used')
	})

# Return all parameters
serialized_all_parameter_return = 	analytics.model('all_parameter_return', {
	'parameters': fields.List(fields.Nested(serialized_parameter_return), description = 'List of parameters')
	})

# Input for updating an existin parameter
serialized_parameter_value = 	analytics.model('parameter_value', {
	'value': fields.String(required=True, description = 'Value of the parameter')
	})