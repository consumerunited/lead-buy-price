# Configuration for a vehicles
from flask_restplus import fields
from analytics.restplus import analytics

from coded_value import coded_value
from identified_value import identified_value
from audit_info import audit_info

vehicle_ownership_enum = ['LEASED','LIEN','OWNED']
vehicle_use_enum = ['COMM_WORK','PLEASURE','BUSINESS','COMM_SCHOOL','FARMING']

vehicle = 	analytics.model('vehicle', {
	'id': fields.Integer(required=True, description = 'ID for the lead'),
	'primary_driver': fields.Nested(identified_value, description = 'The person of the primary driver of the vehicle'),
	'model_year': fields.Nested(identified_value, description = 'The model/year of this vehicle'),
	'model': fields.Nested(coded_value, description = 'The model of this vehicle'),
	'make': fields.Nested(identified_value, description = 'The style of the vehicle if known'),
	'ownership': fields.String(enum = vehicle_ownership_enum, description = 'The type of ownership this vehicle has in relation to the owner of the vehicle'),
	'use': fields.String(enum = vehicle_use_enum, description = 'How is the vehicle use intended'),
	'yearly_miles': fields.Integer(description = 'How many miles is the vehicle driven yearly'),
	'commute_one_way_miles': fields.Integer(description = 'The estimated one way commute miles'),
	'vin': fields.String(max_length = 17),
	'description': fields.String(max_length = 100),
	'created': fields.Nested(audit_info)
	})
