# Configuration for Jornaya Fields Data
from flask_restplus import fields
from analytics.restplus import analytics

jornaya_fields = 	analytics.model('jornaya_fields', {
	"f_name": fields.Integer(descripition = 'Is first name verified '),
	"l_name": fields.Integer(descripition = 'Is last name verified'),
	"email": fields.Integer(descripition = 'Is email verified'),
	"phone1": fields.Integer(descripition = 'Is phone number verified')
	})