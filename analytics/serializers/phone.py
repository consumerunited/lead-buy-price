# Configuration for phone info
from flask_restplus import fields
from analytics.restplus import analytics

type_enum = ['CELL','HOME','WORK']
call_when_enum = ['D','W','N','A']

phone = 	analytics.model('phone', {
	'id': fields.Integer(),
	'number': fields.String(),
	'type': fields.String(enum = type_enum),
	'call_when': fields.String(enum = call_when_enum)
	})
