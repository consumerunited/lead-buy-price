# Configuration for lead score response
from flask_restplus import fields
from analytics.restplus import analytics

lead_tier_enum = ['PLATINUM','GOLD','SILVER','BRONZE']

lead_score_conversion_score = analytics.model('lead_score_conversion' ,{
	'conversion_band': fields.Integer(required = True, description='Forecasted conversion band for lead'),
	'conversion_pred': fields.Fixed(required = True, description = 'Forecasted raw conversion for lead'),
	'model_version': fields.Integer(required = True, description = 'Model version for conversion forecast')
	})

lead_score_response = 	analytics.model('lead_score_response', {
	'id': fields.Integer(required = True, description = 'ID field from inbound message'),
	'lead_tier': fields.String(enum = lead_tier_enum, required= True, description = 'Tier for lead'),
	'conversion': fields.Nested(lead_score_conversion_score, description = 'Factors for conversion', required = True)
	})