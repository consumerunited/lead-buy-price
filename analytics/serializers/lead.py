# Configuration for Lead
from flask_restplus import fields
from analytics.restplus import analytics

from coded_value import coded_value
from identified_value import identified_value
from person import person
from driver import driver
from vehicle import vehicle
from prior_policy import prior_policy
from residence import residence
from audit_info import audit_info
from coverage import coverage
from jornaya import jornaya

lead = 	analytics.model('lead', {
	'id': fields.Integer(required=True, description = 'ID for the lead'),
	'state': fields.String(description = 'Current state of the lead'),
	'attempt': fields.String(description = 'Attempt number on the lead'),
	'status': fields.String(),
	'disposition': fields.Nested(coded_value, description = 'The reason for the current or updated state of the lead'),
	'campaign': fields.Nested(identified_value, description = 'The marketing campaign this lead belongs to'),
	'customer': fields.Nested(identified_value, description = 'The customer this lead was found to belong to or was created from'),
	'contact1': fields.Nested(person, description = 'First contact on the lead'),
	'contact2': fields.Nested(person, description = 'Second contact on the lead'),
	'owner': fields.Nested(identified_value, description = 'Owner of the lead'),
	'dnis': fields.String(description = 'DNIS'),
	'partner_source_code': fields.String(description = 'Source code for the lead'),
	'partner_reference_code': fields.String(description = 'Reference code for the lead'),
	'drivers': fields.List(fields.Nested(driver), description = 'List of drivers on the lead'),
	'vehicles': fields.List(fields.Nested(vehicle), description ='List of vehicles on the lead'),
	'prior_policy': fields.Nested(prior_policy, description = 'Prior policy information'),
	'residence_info': fields.Nested(residence, description = 'Resdience information'),
	'created': fields.Nested(audit_info, description = 'Audit information about when and how the lead was created'),
	'acquisition_date': fields.DateTime(description= 'The date the lead was acquired (i.e. what day we bought it or it was added to the system)'),
	'coverages': fields.List(fields.Nested(coverage), description = 'A list of coverages requested by this lead (i.e. all vehicles)'),
	'jornaya_id': fields.String(description= 'The Jornaya LeadiD for this lead'),
	'jornaya': fields.Nested(jornaya, description = 'The Jornaya DTO for this lead')
	})
