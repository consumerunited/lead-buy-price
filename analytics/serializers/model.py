# Configuration for Model
from flask_restplus import fields
from analytics.restplus import analytics

# What we expect the input to look like
serialized_model = analytics.model('model', {
	'name': fields.String(required=True, description = 'Name of the model'),
	'description': fields.String(description = 'Description of how the model is to be used')
	})

# How we return a single model on a get
serialized_model_return = 	analytics.model('model_return', {
	'id': fields.Integer(required=True, description = 'ID of the model'),
	'name': fields.String(required=True, description = 'Name of the model'),
	'description': fields.String(description = 'Description of how the model is to be used')
	})

# Return all parameters
serialized_all_model_return = 	analytics.model('all_model_return', {
	'models': fields.List(fields.Nested(serialized_model_return), description = 'List of parameters')
	})
