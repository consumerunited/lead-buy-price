# Configuration for answer
from flask_restplus import fields
from analytics.restplus import analytics

from coded_value import coded_value

tier_conversion_info = 	analytics.model('tier_conversion_info', {
	'model_version': fields.Integer(),
	'conversion_band': fields.Integer(),
	'conversion_pred': fields.Fixed()
	})