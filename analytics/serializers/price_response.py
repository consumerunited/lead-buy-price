# Configuration for coverage
from flask_restplus import fields
from analytics.restplus import analytics

conversion = analytics.model('conversion' ,{
	'conversion': fields.Fixed(required = True, description='Forecasted conversion for lead'),
	'short_term_conversion': fields.Fixed(required = True, description = 'Forecasted conversion using short term model'),
	'long_term_conversion': fields.Fixed(required = True, description = 'Forecasted conversion using long term model')
	})

price = analytics.model('price', {
	"bid": fields.Fixed(required = True, decimal = 2, description = 'Price to bid, includes random adjustments to gauge market'),
	"base": fields.Fixed(required = True, decimal = 2, description = 'Price generated using only model outputs, not the price to bid,'),
	})

version = analytics.model('version', {
	'ltv': fields.Integer(required = True, description = 'Version of the LTV Model'),
	'conversion': fields.Integer(required = True, description = 'Version of the conversion model'),
	'demand': fields.Integer(required = True, description = 'Version of the demand model'),
	'market': fields.Integer(required = True, description = 'Version of the market model'),
	'price': fields.Integer(required = True, description = 'Version of the price model')
	})


price_response = 	analytics.model('price_response', {
	'id': fields.Integer(required = True, description = 'ID field from inbound message'),
	'conversion': fields.Nested(conversion, description = 'Factors for conversion', required = True),
	'ltv': fields.Fixed(required = True, description = 'Forecasted LTV for lead'),
	'demand': fields.Fixed(required = True, description = 'Current demand factor'),
	'market': fields.Fixed(required = True, description = 'Current market factor'),
	'price': fields.Nested(price, description = 'Price components', required = True),
	'version': fields.Nested( version, description = 'Versions of the various modesl', required = True),
	'margin': fields.Fixed(required = True, description='Margin that we want out of the lead (i.e. marketing CAC)'),

	})
