# Configuration for answer
from flask_restplus import fields
from analytics.restplus import analytics

from coded_value import coded_value

answer = 	analytics.model('answer', {
	'question': fields.Nested(coded_value, description = 'The reference to the question this answer is for'),
	'answer': fields.String(description = 'The answer to this question')

	})