# Configuration for audit info
from flask_restplus import fields
from analytics.restplus import analytics

from identified_value import identified_value

audit_info = 	analytics.model('audit_info', {
	'at': fields.DateTime(required = True, description = 'The date and time the action occurred'),
	'by': fields.String(required = True, description = 'Who and what system did this action'),
	'employee' : fields.Nested(identified_value, description = 'The Employee corresponding to the Employee that did this action')
	})
