# Configuration for duplicate response info
from flask_restplus import fields
from analytics.restplus import analytics

yes_no_enum = ['Y','N']

duplicate_response = 	analytics.model('duplicate_response', {
	'id': fields.Integer(description = "Lead ID"),
	'active_policy': fields.String(enum = yes_no_enum, description = 'Yes or no indicator for when we have an active policy for this lead'),
	'assigned_employee': fields.Integer(description = 'Employee ID for agent assigned to lead'),
	'customers': fields.List(fields.Integer(), description = 'List of customers associated with lead'),
	'leads': fields.List(fields.Integer(),description = 'List of leads associated with lead'),
	'new_phone': fields.String(enum = yes_no_enum, description = 'Yes or no indicator for when we are recieving a new phone number for an existing person'),
	'persons': fields.List(fields.Integer(),description = 'List of persons associated with lead'),
	'policies': fields.List(fields.Integer(),description = 'List of policies assoicated with lead'),
	'recent_purchase': fields.String(enum = yes_no_enum, description = 'Yes or no indicator of if Goji has purchased a lead for this individual in the past 120 Days'),
	'reject': fields.String(enum = yes_no_enum, description = 'Yes or no indicator for when to reject lead')
	})
