# Configuration for answer
from flask_restplus import fields
from analytics.restplus import analytics

from coded_value import coded_value
from tier_conversion_info import tier_conversion_info

tier_info = 	analytics.model('tier_info', {
	'tier': fields.Nested(coded_value, description = 'The actual tier of the lead/customer'),
	'conversion': fields.Nested(tier_conversion_info)
	})