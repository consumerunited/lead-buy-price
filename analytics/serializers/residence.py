# Configuration for residence
from flask_restplus import fields
from analytics.restplus import analytics

from address import address

home_ownership_enum = ['APARTMENT','CONDO_OWNED','HOME_OWNED','RENTAL','WITH_PARENT','MOBILE_HOME','OTHER']
basement_type_enum = ['BASEMENT_FULLY_FINISHED','BASEMENT_THREE_QUARTERS_FINISHED','BASEMENT_ONE_HALF_FINISHED','BASEMENT_ONE_QUARTER_FINISHED','BASEMENT_UNFINISHED','CRAWL_SPACE','PIERS_PILINGS_STILTS','SLAB','OTHER']
distance_to_brush_enum = ['M0_05','M05_1','MORE_THAN_1']
distance_to_tidal_water_enum = ['M0_05','M05_1','M1_2','M2_5','MORE_THAN_5']
construction_class_enum = ['STANDARD','LUXURY','ECONOMICAL','MANUFACTURED']
construction_style_enum = ['APARTMENT','BACKSPLIT','BI_LEVEL','BI_LEVEL_ROW_CENTER','BI_LEVEL_ROW_END','BUNGALOW','CAPE_COD','COLONIAL','CONDO','COOP','CONTEMPORARY','COTTAGE','DWELLING','FEDERAL_COLONIAL','MEDITERRANEAN','ORNATE_VICTORIAN','QUEEN_ANNE','RAISED_RANCH','RAMBLER','RANCH','ROWHOUSE','ROWHOUSE_CENTER','ROWHOUSE_END','SOUTHWEST_ADOBE','SPLIT_FOYER','SPLIT_LEVEL','SUBSTANDARD','TOWNHOUSE','TOWNHOUSE_CENTER','TOWNHOUSE_END','TRI_LEVEL','TRI_LEVEL_CENTER','VICTORIAN']
construction_type_enum = ['WOOD_FRAME','METAL_FRAME','MASONRY','LOG_HOME','OTHER']
exterior_walls_enum = ['ADOBE','ALUMINUM_VINYL','BARN_PLANK','BRICK','BRICK_ON_BLOCK','BRICK_ON_BLOCK_CUSTOM','BRICK_VENEER','BRICK_VENEER_CUSTOM','CEMENT_FIBER_SHINGLES','CLAPBOARD','CONCRETE_DECORATIVE_BLOCK_PAINTED','EXTERIOR_INSULATION_AND_FINISH_SYSTEM','FIRE_RESISTANT','FRAME','LOGS','POURED_CONCRETE','SIDING_ALUMINUM','SIDING_HARDBOARD','SIDING_PLYWOOD','SIDING_STEEL','SIDING_T111','SIDING_VINYL','SIDING_WOOD','SLUMP_BLOCK','SOLID_BRICK','SOLID_BRICK_CUSTOM','SOLID_BROWNSTONE','SOLID_STONE','SOLID_STONE_CUSTOM','STONE_ON_BLOCK','STONE_ON_BLOCK_CUSTOM_STONE','STONE_VENEER','STONE_VENEER_CUSTOM','STUCCO','STUCCO_ON_BLOCK','STUCCO_ON_FRAME','VICTORIAN_SCALLOPED_SHAKES','WINDOW_WALLS','WOOD_SHAKES','CINDER_BLOCK','OTHER']
garage_type_enum = ['NO_GARAGE','CARPORT_ATTACHED','CARPORT_DETACHED','ONE_CAR_ATTACHED','TWO_CAR_ATTACHED','THREE_CAR_ATTACHED','ONE_CAR_DETACHED','TWO_CAR_DETACHED','THREE_CAR_DETACHED','ONE_CAR_BUILT_IN','TWO_CAR_BUILT_IN','THREE_CAR_BUILT_IN','OTHER']
heating_type_enum = ['ELECTRIC','GAS','GAS_FORCED_AIR','GAS_HOT_WATER','OIL','OIL_FORCED_AIR','OIL_HOT_WATER','SOLID_FUEL','BOILER','OIL_COAL_KERONSENE','PROPANE','STOVE','OTHER','UNKNOWN']
feet_from_hydrant_enum = ['F1_500','F501_600','F601_1000','F1001_1100','F1201_1300','F1301_1400','F1401_1500','F1501_AND_GREATER','NONE_IN_AREA']
dwelling_usage_enum = ['PRIMARY','SECONDARY','SEASONAL','FARM','UNOCCUPIED','VACANT','COC','VACATION','TENANT_OCCUPIED']
dwelling_type_enum = ['ONE_FAMILY','TWO_FAMILY','THREE_FAMILY','FOUR_FAMILY']
panel_type_enum = ['CIRCUIT_BREAKER','FUSE_BOX','OTHER','UNKNOWN']
property_type_enum = ['SINGLE_FAMILY_HOME','MULTI_FAMILY_HOME','APARTMENT','DUPLEX','CONDOMINIUM','TOWNHOME','MOBILE_HOME','OTHER']
roof_type_enum = ['ARCHITECTURAL_SHINGLES','ASBESTOS','ASPHALT_SHINGLES', 'COMPOSITION', 'COPPER_FLAT', 'COPPER_PITCHED', 'CORRUGATED_STEEL_FLAT', 'CORRUGATED_STEEL_PITCHED', 'FIBERGLASS', 'FOAM', 'GRAVEL', 'METAL_FLAT', 'METAL_PITCHED', 'MINERAL_FIBER_SHAKE', 'PLASTIC_FLAT', 'PLASTIC_PITCHED', 'ROCK', 'ROLLED_PAPER_FLAT', 'ROLLED_PAPER_PITCHED', 'RUBBER_FLAT', 'RUBBER_PITCHED', 'SLATE','TAR', 'TAR_AND_GRAVEL', 'TILE_CLAY', 'TILE_CONCRETE', 'TILE_SPANISH', 'TIN_FLAT', 'TIN_PITCHED', 'WOOD_FIBERGLASS_SHINGLES', 'WOOD_SHAKE', 'WOOD_SHINGLES', 'CONCRETE', 'OTHER']
security_system_enum = ['NONE', 'MONITORED','UNMONITORED','UNSURE']
story_count_enum = ['S1','S1_5','S2','S2_5', 'S3','S4','BI_LEVEL','TRI_LEVEL']
wiring_type_enum = ['COPPER','ALUMINUM', 'KNOB_AND_TUBE', 'UNKNOWN']
protection_class_enum = ['P1','P2','P3','P4','P5','P6','P7','P8','P9','P10']

residence = 	analytics.model('residence', {
	'address': fields.Nested(address, description = 'The address of the residence'),
	'claim_count': fields.Integer(description = 'Number of claims made'), 
	'home_ownership': fields.String(enum = home_ownership_enum, description = 'Do they own the home, rent or other'),
	'residence_current_length_months': fields.Integer(description = 'How many months have they lived at the home'),
	'residence_previous_length_months': fields.Integer(description = 'How many months have they lived at their previous home'),
	'basement_type': fields.String(enum = basement_type_enum, description = 'What type of basement is in the home'),
	'bathroom_count': fields.Integer(description = 'How many full bathrooms are in the home'),
	'half_bathroom_count': fields.Integer(description = 'How many half bathrooms are in the home'),
	'three_quarter_bathroom_count': fields.Integer(description = 'How many three quarter bathrooms are in the home'),
	'bedroom_count': fields.Integer(description = 'How many bedrooms are in the home'),
	'distance_to_brush': fields.String(enum = distance_to_brush_enum, description = 'How far are any brush fire hazards'),
	'distance_to_tidal_water': fields.String(enum = distance_to_tidal_water_enum, description = 'How far are tidal waters like the ocean or rivers that ebb and flow with the tide'),
	'year_built': fields.Integer(description = 'What year was the home built'),
	'business_or_farming': fields.Boolean(description = 'Is the home used for business or farming'),
	'central_air': fields.Boolean(description = 'Does the home have central air conditioning'),
	'construction_class': fields.String(enum = construction_class_enum, description = 'What class of construction is the home, eg: standard, luxury, economy'),
	'construction_style': fields.String(enum = construction_style_enum, description = 'What style of construction is the home, eg colonial, ranch etc'),
	'construction_type': fields.String(enum = construction_type_enum, description = 'What type of construction is the home, eg: wood, metal, masonry'),
	'copper_water_pipes': fields.Boolean(description = 'Does the home have copper water pipes'),
	'dangerous_dog': fields.Boolean(description = '"Does the home owner have dangerous dogs. '),
	'deadbolt': fields.Boolean(description = 'Does the home have a deadbolt'),
	'deck': fields.Boolean(description = 'Does the home have a deck'),
	'exterior_walls': fields.String(enum = exterior_walls_enum, description = 'What are the exterior walls of the home built with'),
	'fire_extinquisher': fields.Boolean(description = 'Does the home have a fire extinguisher'),
	'miles_from_fire_station': fields.Integer(description = 'How many miles away is the fire station'),
	'fireplace': fields.Boolean(description = 'Does the home have a fireplace'),
	'flood_area': fields.Boolean(description = 'Is the home in a flood area'),
	'garage_type': fields.String(enum = garage_type_enum, description = 'What kind of garage does the home have'),
	'heating_type': fields.String(enum = heating_type_enum, description = 'What kind of heating does the home have'),
	'hot_tub': fields.Boolean(description = 'Does the home have a hot tub'),
	'feet_from_hydrant': fields.String(enum=feet_from_hydrant_enum, description = 'How many feet away is the closes fire hydran'),
	'indoor_sprinklers': fields.Boolean(description= 'Does the home have indoor sprinklers'),
	'mobile_home': fields.Boolean(description = 'Does the home have indoor sprinklers'),
	'new_purchase': fields.Boolean(description = 'Is this a new home purchase'),
	'dwelling_usage': fields.String(enum = dwelling_usage_enum, description = 'What is this dwelling usage'),
	'dwelling_type': fields.String(enum = dwelling_type_enum, description = 'What is this dwelling type'),
	'panel_type': fields.String(enum = panel_type_enum, description = 'What is this homes electical panel type'),
	'property_type': fields.String(enum = property_type_enum, description = 'What is this homes property type'),
	'property_value': fields.Integer(description = 'What is this homes property value'),
	'roof_type': fields.String(enum = roof_type_enum, description = 'What is this homes roof type'),
	'roof_age': fields.String(description ='What is this homes roof age'),
	'sauna': fields.Boolean(description = 'Does the house have a sauna'),
	'security_system': fields.String(enum = security_system_enum, description = 'What is this homes security system type'),
	'smoke_detector': fields.Boolean(description = 'Does this home have smoke detectors'),
	'smoker_in_household': fields.Boolean(description = 'Do any smokers live in the household'),
	'square_footage': fields.Integer(description = 'What is this homes square footage'),
	'story_count': fields.String(enum = story_count_enum, description = 'How many stories is this home'),
	'sump_pump': fields.Boolean(description = 'Does this home have a sump pump'),
	'swimming_pool': fields.Boolean(description = 'Does this home have a swimming pool'),
	'swimming_pool_fence': fields.Boolean(description = 'If there is a swimming pool is there a fence'),
	'trampoline': fields.Boolean(description = 'Does this house have a trampoline'),
	'unit_count': fields.String(description = 'How many units does this house have'),
	'upgrade_year': fields.Integer(description = 'What year was this home upgraded'),
	'wiring_type': fields.String(enum = wiring_type_enum, description = 'What kind of wiring does this home have'),
	'wood_stove': fields.Boolean(description = 'Does this home have a wood stove'),
	'home_under_construction': fields.Boolean(description = 'Is thie home under construction'),
	'within_city_limits': fields.Boolean(description = 'Is this home within city limits'),
	'within_fire_district': fields.Boolean(description = 'Is this home within a fire district'),
	'protection_class': fields.String(enum = protection_class_enum, description = 'The protection class')
	})









