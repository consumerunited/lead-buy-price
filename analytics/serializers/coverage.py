# Configuration for coverage
from flask_restplus import fields
from analytics.restplus import analytics

from coded_value import coded_value

coverage = 	analytics.model('coverage', {
	'type': fields.Nested(coded_value, description = 'The type of this coverage'),
	'value': fields.String(description = 'The value for this coverage'),
	'rate': fields.Float(description = 'The amount (as part of the total policy premium) this specific coverage costs on the policy'),
	'deductible': fields.String(description = 'the deductible on this coverage if any'),
	'deductible2': fields.String(description = 'The 2nd deductible on this coverage if any'),
	'deductible3': fields.String(description = 'The 3rd deductible on this coverage if any'),
	'description': fields.String(description = 'A description of this value')
	})
