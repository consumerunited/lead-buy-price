# Configuration for coded value
from flask_restplus import fields
from analytics.restplus import analytics


coded_value = 	analytics.model('coded_value', {
	'code': fields.String(description = 'The unique string identifier for this value'),
	'description': fields.String(description = 'A description of this value. Only specified if name doesnt give enough information to identify this value'),
	'name': fields.String(description = 'A descriptive name of this value'),
	'order': fields.Integer(description = 'Same types of entities have an order associated with them that allows sorting. If present implies the value shoudl be sorted according to this integer value ascending.')
	})
