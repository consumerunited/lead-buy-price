# Configuration for Lead
from flask_restplus import fields
from analytics.restplus import analytics

# What we expect the input to look like
serialized_support_data_value = 	analytics.model('support_data_value', {
	'key': fields.String(required=True, description = 'Key to lookup value'),
	'value': fields.Float(required=True, description = 'Value associated with the key')
	})

# How we return a single parameter on a get
serialized_support_data_value_return = 	analytics.model('support_data_value_return', {
	'support_data_id': fields.Integer(required=True, description = 'ID of the support data type this value is associated with'),
	'support_data_value_id': fields.Integer(required=True, description = 'ID of the support data value'),
	'key': fields.String(required=True, description = 'Key to lookup value'),
	'value': fields.Float(required=True, description = 'Value associated with the key')
	})

# Return all parameters
serialized_all_support_data_value_return = 	analytics.model('all_support_data_value_return', {
	'support_data_values': fields.List(fields.Nested(serialized_support_data_value_return), description = 'List of parameters')
	})

# Input for updating an existin parameter
serialized_support_data_value_up = 	analytics.model('data_value', {
	'value': fields.Float(required=True, description = 'Value of the support data')
	})


