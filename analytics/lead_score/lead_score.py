import logging
import os
import sys
import json as jsn

from flask import request, jsonify
from flask_restplus import Resource
from analytics.serializers.lead import lead
from analytics.serializers.lead_score_response import lead_score_response
from analytics.restplus import analytics

# Parse data out of lead
from analytics.get_price import parse_lead_request

# Scoring models
from models.lead_score.conversion import get_conversion

log = logging.getLogger(__name__)

ns = analytics.namespace('lead_score')

@ns.route('/')
class get_score(Resource):

    @analytics.expect(lead)
    @analytics.marshal_with(lead_score_response, code = 201)
    def post(self):
    	"""
        Given a lead return a score band
        """        

        # Get data from lead
    	data = request.json

    	# Parse data
        parsed_lead_data, zip_data, conversion_data,jornaya_data = parse_lead_request.transform_lead(request.json)
        lead_id = request.json['id']

        # Make Conversion Forecast
        conversion_pred, conversion_band, model_version = get_conversion.get_conversion(parsed_lead_data, zip_data, conversion_data )
        conversion_response = {'conversion_band':conversion_band,'conversion_pred':round(conversion_pred,6), 'model_version': model_version}

        # Create score band
        if conversion_band > 85 and parsed_lead_data['vehicles'] > 1:
            lead_score_band = 'PLATINUM'
        elif (conversion_band > 45 and parsed_lead_data['vehicles'] > 1) or (conversion_band > 80):
            lead_score_band = 'GOLD'
        elif conversion_band > 33:
            lead_score_band = 'SILVER'
        else:
            lead_score_band = 'BRONZE'

        response = {'id':lead_id,'lead_tier':lead_score_band,'conversion':conversion_response}

        return response, 201
