
import os
import random 
import pickle
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

standardize_data = pickle.load(open(os.environ['APP_ROOT'] + '/models/customer_recommendation/version_1/standardize.pkl','rb'))
predict_cluster = pickle.load(open(os.environ['APP_ROOT'] + '/models/customer_recommendation/version_1/cluster.pkl','rb'))
cluster_summary_data = pd.read_csv(os.environ['APP_ROOT'] + "/models/customer_recommendation/version_1/cluster_summary.csv")

def generate_value_dict():
	values_dict = {}
	values_dict['mean_premium'] = random.randint(50,200)
	values_dict['median_premium'] = values_dict['mean_premium'] 
	values_dict['stddev_premium'] = random.randint(5,30)
	values_dict['third_quartile_premium'] = round(values_dict['mean_premium'] + 1.75*values_dict['stddev_premium'],0)
	values_dict['first_quartile_premium'] = round(values_dict['mean_premium'] - 1.75*values_dict['stddev_premium'],0)
	values_dict['group_coverage_per'] = random.random()
	values_dict['confidence'] = 'NA'
	values_dict['conversion'] = -1
	values_dict['score'] = values_dict['mean_premium'] * values_dict['group_coverage_per']


	return values_dict

def create_product_dict(product_type, product_data):
	product_dict = {}

	product_dict["mean_premium"] = int(product_data[product_type + "_mean"])
	product_dict["median_premium"] = int(product_data[product_type + "_med"])
	product_dict["stddev_premium"] = int(product_data[product_type + "_sd"])
	product_dict["first_quartile_premium"] = int(product_data[product_type + "_25_quan"])
	product_dict["third_quartile_premium"] = int(product_data[product_type + "_75_quan"])
	product_dict["group_coverage_per"] = round(product_data[product_type + "_perc"],3)
	product_dict["score"] = product_dict["mean_premium"] * product_dict["group_coverage_per"]

	return product_dict

def generate_recommendation(customer_list):

	# Get Cluster
	value = np.array(customer_list)
	standardized = standardize_data.transform(value.reshape(1, -1))
	cluster = predict_cluster.predict(standardized)
	distance_z = abs(np.linalg.norm(standardized-predict_cluster.cluster_centers_[cluster[0]]) - 0.5700300561313832)/0.13613532746854745

	# Cluster Summary
	cluster_product_premium = cluster_summary_data[cluster_summary_data.customer_group == cluster[0]].iloc[0]
	products = ['auto','home','pumb','rent','boat','cond','cycl','fire','paf']
	product_list = []

	for product in products:
		try:
			product_list.append({"product_type":product.upper(), "product_type_values":create_product_dict(product, cluster_product_premium)})
		except:
			pass
			
	# Finish it up	
	recommendation = {}
	recommendation['recommendations'] = product_list

	return recommendation
