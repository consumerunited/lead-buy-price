import os
import datetime
import re
import json
import requests
from datetime import date
import pandas as pd

headers = {
    'authorization': "Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIzIiwiZmlyc3RuYW1lIjoiQW5hbHl0aWNzIiwicm9sZXMiOlsiUk9MRV9BRE1JTiIsIlJPTEVfTUFOQUdFUiIsIlJPTEVfVVNFUiJdLCJleHAiOjIwMTg2NTg5ODQsImlhdCI6MTUxODY1ODk4NCwiZW1haWwiOiJzZXItYW5hbHl0aWNzQGdvamkuY29tIiwibGFzdG5hbWUiOiJTZXJ2aWNlIn0.UQ_000SdJGVwHReTKqrsgexjksj8kkFp8btPatRNjWFrtiYQM1xhNIV8_n-fVUnoQk2LAsIwQgfQPA9OSu_iTuEeBC07kP5rYPoCVm-yJlfGZSiNbSNNEH3B81s3ITY2bTG62WHI7tK9GQu41d2XP4gok-ah3y1x6HpQMU4mevRsURDqDYZDcHEanCTdpAKxdbjU7ZH2krE2HZMxEZbWH7-AE3L7QUghm7I_CPjyP84P8KFoJaupXU1oKZX60u_3zfsdf075HXoS_u_5-lqO3zjZyQXPLTOkchw0ExPqmWncApWWxDeNQuiG9aB3DzAbLlJbSC4gEmtRPgpmnMhRCg",
    'cache-control': "no-cache"
     }

# Create data frame for zip code data
zip_raw_data = pd.read_csv(os.environ['APP_ROOT'] + "/models/normalizers/zip_code_normalizers/zip_code_demographics.csv",dtype={'zip':object})

class Error(Exception):
   """Base class for other exceptions"""
   pass

class NoAddressForCustomer(Error):
   """Raised when there is no address data for customer"""
   pass

class UnknownZip(Error):
   """Raised when there is no address data for customer"""
   pass



def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))


def get_customer_data(customer_id):

	# Get Customer Data
    customer_url = "https://developer.goji.com/crm/customer/%s"

    customer_response = requests.request("GET", customer_url%(customer_id), headers=headers)
    customer_dict = json.loads(customer_response.text)

    # Get Policy data
    policy_url = "https://developer.goji.com/crm/customer/%s/policy"
    
    policy_response = requests.request("GET", policy_url%(customer_id), headers=headers)
    policy_dict = json.loads(policy_response.text)    
    
    # Customer Data Dict
    rec_data_dict = {}
    primary_contact = customer_dict['contact1']
    
    # Customer ID
    rec_data_dict['customer_id'] = customer_dict['id']
    
    # Zip Code
    try:
        rec_data_dict['zip_code'] = primary_contact['addresses'][0]['zip_code'][:5]
    except:
        raise NoAddressForCustomer("No address data for customer")
        
    # Age
    if 'date_of_birth' in primary_contact:
        rec_data_dict['age'] = calculate_age(datetime.datetime.strptime(primary_contact['date_of_birth'],'%Y-%m-%d').date())
    else:    
        rec_data_dict['age'] = 0
    # Years Liscenced
    if 'date_first_licensed' in primary_contact:
        rec_data_dict['years_liscenced'] = calculate_age(datetime.datetime.strptime(primary_contact['date_first_licensed'],'%Y-%m-%d').date())
    else:
        rec_data_dict['years_liscenced'] = 0
        
    # Phone Types
    rec_data_dict['cell_phone'] = 0
    rec_data_dict['home_phone'] = 0
    rec_data_dict['work_phone'] = 0
    
    if 'phones' in primary_contact:
        for phone in primary_contact['phones']:
            if 'type' in phone:
                if phone['type'] == 'HOME':
                    rec_data_dict['home_phone'] = 1
                elif phone['type'] == 'CELL':
                    rec_data_dict['cell_phone'] = 1
                elif phone['type'] == 'WORK':
                    rec_data_dict['work_phone'] = 1
    
    # Education
    rec_data_dict['no_education'] = 0
    rec_data_dict['HS_education'] = 0
    rec_data_dict['higher_education'] = 0
    
    if 'education' in primary_contact:
        if primary_contact['education'] == 'NONE' or primary_contact['education'] is None:
            rec_data_dict['no_education'] = 1
        elif primary_contact['education'] == 'HIGH_SCHOOL' or primary_contact['education'] == 'GED':
            rec_data_dict['HS_education'] = 1
        else:
            rec_data_dict['higher_education'] = 1
    
    # Gender
    rec_data_dict['male'] = 0
    rec_data_dict['female'] = 0
    
    if 'gender' in primary_contact:
        if primary_contact['gender'] == 'M':
            rec_data_dict['male'] = 1
        elif primary_contact['gender'] == 'F':
            rec_data_dict['female'] = 1
    
    # Marital Status
    rec_data_dict['married'] = 0
    rec_data_dict['single'] = 0
    rec_data_dict['widowed'] = 0
    rec_data_dict['divorced'] = 0
    
    if 'marital_status' in primary_contact:
        if primary_contact['marital_status'] == 'MARRIED':
            rec_data_dict['married'] = 1
        elif primary_contact['marital_status'] == 'SINGLE':
            rec_data_dict['single'] = 1
        elif primary_contact['marital_status'] == 'WIDOWED':
            rec_data_dict['widowed'] = 1
        elif primary_contact['marital_status'] == 'DIVORCED':
            rec_data_dict['divorced'] = 1
            
    # License Status
    rec_data_dict['license_status'] = 0
    
    if 'license_status' in primary_contact:
        if primary_contact['license_status'] == 'VALID':
            rec_data_dict['license_status'] = 1
            
    # Home Owner
    rec_data_dict['home_owned'] = 0
    rec_data_dict['apartment'] = 0
    rec_data_dict['condo_owned'] = 0
    rec_data_dict['mobile_home'] = 0
    rec_data_dict['rental'] = 0
    rec_data_dict['with_parent'] = 0
    
    if 'home_ownership' in primary_contact:
        if primary_contact['home_ownership'] == 'HOME_OWNED':
            rec_data_dict['home_owned'] = 1
        elif primary_contact['home_ownership'] == 'APARTMENT':
            rec_data_dict['apartment'] = 1
        elif primary_contact['home_ownership'] == 'CONDO_OWNED':
            rec_data_dict['condo_owned'] = 1            
        elif primary_contact['home_ownership'] == 'MOBILE_HOME':
            rec_data_dict['mobile_home'] = 1                       
        elif primary_contact['home_ownership'] == 'RENTAL':
            rec_data_dict['rental'] = 1       
        elif primary_contact['home_ownership'] == 'WITH_PARENT':
            rec_data_dict['with_parent'] = 1       
        
    # Aggregate Policy Driver Features
    driver_set = set()
    max_age = 0
    min_age = 1000
    max_yrs_licensed = 0
    min_yrs_licensed = 1000
    rated_cnt = 0
    child_cnt = 0
    parent_cnt = 0
    relative_cnt = 0
    spouse_cnt = 0
    male_cnt = 0
    female_cnt = 0
    
    for policy in policy_dict:
        if 'drivers' in policy and policy['status'] == 'ACTIVE':
            for driver in policy['drivers']: 
                
                # Unique list of drivers for total drivers 
                driver_set.add(driver['id'])
                
                # Max and Min Age
                if 'date_of_birth' in driver:
                    driver_age = calculate_age(datetime.datetime.strptime(driver['date_of_birth'],'%Y-%m-%d').date())
                else:    
                    driver_age = 0
                    
                if driver_age > max_age:
                    max_age = driver_age
                if driver_age < min_age:
                    min_age = driver_age

                # Max and Min Years Licnsed
                if 'date_first_licensed' in driver:
                    years_licensed = calculate_age(datetime.datetime.strptime(driver['date_first_licensed'],'%Y-%m-%d').date())
                else:    
                    years_licensed = 0
                    
                if years_licensed > max_yrs_licensed:
                    max_yrs_licensed = years_licensed
                if years_licensed < min_yrs_licensed:
                    min_yrs_licensed = years_licensed
                    
                    
                # Driver type
                if 'rating' in driver:
                    if driver['rating'] == 'RATED':
                        rated_cnt += 1
                    elif driver['rating'] == 'CHILD':
                        child_cnt += 1
                    elif driver['rating'] == 'PARENT':
                        parent_cnt += 1
                    elif driver['rating'] == 'RELATIVE':
                        relative_cnt += 1
                    elif driver['rating'] == 'SPOUSE':
                        spouse_cnt += 1
                        
                # Driver Gender
                if 'gender' in driver:
                    if driver['gender'] == 'M':
                        male_cnt += 1
                    elif driver['gender'] == 'F':
                        female_cnt += 1
                        
    if min_age == 1000:
        min_age = 0
    if min_yrs_licensed == 1000:
        min_yrs_licensed = 0
    
    rec_data_dict['num_drivers'] = len(driver_set)  
    rec_data_dict['min_age'] = min_age
    rec_data_dict['max_age'] = max_age
    rec_data_dict['max_years_liscenced'] = max_yrs_licensed
    rec_data_dict['min_years_liscenced'] = min_yrs_licensed
    rec_data_dict['RATED'] = rated_cnt
    rec_data_dict['CHILD'] = child_cnt
    rec_data_dict['PARENT'] = parent_cnt
    rec_data_dict['RELATIVE'] = relative_cnt
    rec_data_dict['SPOUSE'] = spouse_cnt
    rec_data_dict['female_cnt'] = female_cnt
    rec_data_dict['male_cnt'] = male_cnt
    
    # Aggregate Policy Vehicle Features
    vehicle_set = set()
    vehicle_cnt = 0
    max_value = 0
    min_value = 1000000
    total_value = 0
    max_est_yearly_miles = 0
    min_est_yearly_miles = 1000000
    total_est_yearly_miles = 0
    truck_num = 0
    car_num = 0
    van_num = 0
    minivan_num = 0
    suv_num = 0
    
    for policy in policy_dict:
        if 'vehicles' in policy and policy['status'] == 'ACTIVE':
            for vehicle in policy['vehicles']: 
                
                # Unique Vehicles
                vehicle_set.add(vehicle['id'])
                
                # Count of vehicles for averages
                vehicle_cnt += 1
                
                # Estimated Miles
                if 'yearly_miles' in vehicle:
                    
                    if vehicle['yearly_miles'] > max_est_yearly_miles:
                        max_est_yearly_miles = vehicle['yearly_miles']
                    if vehicle['yearly_miles'] < min_est_yearly_miles:
                        min_est_yearly_miles = vehicle['yearly_miles']
                    total_est_yearly_miles += vehicle['yearly_miles']
                
                # Get Vehicle Style Data
                if 'style' in vehicle and 'id' in vehicle['style']:
                    vehicle_style_url = 'https://developer.goji.com/crm/vehicle/style/%s'
                    
                    vehicle_style_response = requests.request("GET", vehicle_style_url%(vehicle['style']['id']), headers=headers)
                    vehicle_style_dict = json.loads(vehicle_style_response.text)   
                    
                    # Vehicle Value
                    if 'used_tmv_retail' in vehicle_style_dict['price']:
                        if vehicle_style_dict['price']['used_tmv_retail'] > max_value:
                            max_value = vehicle_style_dict['price']['used_tmv_retail']
                        if vehicle_style_dict['price']['used_tmv_retail'] < min_value:
                            min_value = vehicle_style_dict['price']['used_tmv_retail']
                        total_value += vehicle_style_dict['price']['used_tmv_retail']
                    else:
                        pass
                    
                    # Vehicle Type
                    if 'vehicle_type' in vehicle_style_dict['categories']:
                        if vehicle_style_dict['categories']['vehicle_type'].upper() == 'TRUCK':
                            truck_num += 1
                        elif vehicle_style_dict['categories']['vehicle_type'] == 'CAR':
                            car_num += 1         
                        elif vehicle_style_dict['categories']['vehicle_type'] == 'VAN':
                            van_num += 1 
                        elif vehicle_style_dict['categories']['vehicle_type'] == 'MINIVAN':
                            minivan_num += 1                         
                        elif vehicle_style_dict['categories']['vehicle_type'] == 'SUV':
                            suv_num += 1      
                    else:
                        pass
                        
    rec_data_dict['vehicle_num'] = len(vehicle_set)  
    rec_data_dict['max_value'] = max_value
    rec_data_dict['min_value'] = min_value
    if vehicle_cnt == 0:
        rec_data_dict['avg_value'] = 0
        rec_data_dict['avg_est_yearly_miles'] = 0
    else:
        rec_data_dict['avg_value'] = total_value/vehicle_cnt
        rec_data_dict['avg_est_yearly_miles'] = total_est_yearly_miles/vehicle_cnt
    rec_data_dict['max_est_yearly_miles'] = max_est_yearly_miles
    rec_data_dict['min_est_yearly_miles'] = min_est_yearly_miles
    rec_data_dict['truck_num'] = truck_num
    rec_data_dict['car_num'] = car_num
    rec_data_dict['van_num'] = van_num
    rec_data_dict['minivan_num'] = minivan_num
    rec_data_dict['suv_num'] = suv_num
    
    # Zip Data
    try:
        full_zip_data = zip_raw_data[zip_raw_data.zip == rec_data_dict['zip_code']].iloc[0]
    except:
        if rec_data_dict['zip_code'] == '75072':
            rec_data_dict['zip_code'] = '75069'
            full_zip_data = zip_raw_data[zip_raw_data.zip == rec_data_dict['zip_code']].iloc[0]
        else:
            raise UnknownZip("No data for this customers zip code")

    rec_data_dict['overall_bach_or_hgher'] = full_zip_data['overall_bach_or_hgher']   
    rec_data_dict['median_earning'] = full_zip_data['median_earning']   
    rec_data_dict['urban_pop_per'] = full_zip_data['urban_pop_per']   
    rec_data_dict['oo_value_median'] = full_zip_data['oo_value_median']   
    rec_data_dict['mean_commute_time'] = full_zip_data['mean_commute_time']   
    rec_data_dict['over_62_pop_per'] = full_zip_data['over_62_pop_per']   

    # Rec Data List
    rec_data_list = []

    rec_data_list.append(rec_data_dict['age'])
    rec_data_list.append(rec_data_dict['married'])
    rec_data_list.append(rec_data_dict['overall_bach_or_hgher'])
    rec_data_list.append(rec_data_dict['median_earning'])
    rec_data_list.append(rec_data_dict['urban_pop_per'])
    rec_data_list.append(rec_data_dict['oo_value_median'])
    rec_data_list.append(rec_data_dict['mean_commute_time'])
    rec_data_list.append(rec_data_dict['over_62_pop_per'])

    return rec_data_dict, rec_data_list
