
import logging
import os
import sys
import json as jsn

from flask import request, jsonify
from flask_restplus import Resource
from analytics.restplus import analytics
from analytics.serializers.customer_recommendation_response import customer_recommendation_response

from get_customer_data import get_customer_data
from generate_recommendation import generate_recommendation

log = logging.getLogger(__name__)

ns = analytics.namespace('customer_recommendation')

# Class for processing a posted customer id
@ns.route('/<int:customer_id>/')
class get_customer_rec(Resource):
 	@analytics.marshal_with(customer_recommendation_response)
	def get(self,customer_id):
		"""
		Given a Goji customer will return recommendations for that customer
		"""  

		# Get customer data from dev APIs
		customer_dict, customer_list = get_customer_data(customer_id)

		# Generate Recomendation
		recommendation = generate_recommendation(customer_list) 
		recommendation['id'] = customer_id
		return recommendation,200


