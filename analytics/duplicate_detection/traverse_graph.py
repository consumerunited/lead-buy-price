## Given a starting key traverse the graph and find all the persons
from pyArango.connection import Connection
import datetime
import re
import json
import requests


def find_persons_in_graph(start_key, arango_dict):     

    conn = Connection(  arangoURL=arango_dict['arango_url'],
                    username=arango_dict['arango_user'],
                    password=arango_dict['arango_password'])

    db = conn[arango_dict['arango_db_name']]

    keyCollection = db.collections["key"]
    personCollection = db.collections["person"]

    g = db.graphs["customer"]

    keys_to_visit = set()
    visited_keys = set()
    
    visited_persons = set()
              
    keys_to_visit.add(start_key) 
    visited_keys.add(start_key) 
    
    while len(keys_to_visit) != 0:
        start_key = list(keys_to_visit)[0]
        
        try:
            key = keyCollection[start_key]
            direction = 'outbound'
        except:
            key = personCollection[start_key]      
            direction = 'inbound'
        
        # Query Graph to get start point
        start_results = g.traverse(key, direction = direction)
        
        for vertex in start_results['visited']['vertices']:
            if vertex['_key'] != start_key:
                if vertex['_key'] not in visited_keys:
                    keys_to_visit.add(vertex['_key'])
                if vertex['_key'] not in visited_persons and 'person' in vertex['_id']:
                    visited_persons.add(vertex['_key'])
        
        if start_key not in visited_keys:
            visited_keys.add(start_key)

        keys_to_visit.remove(start_key)
        
    return visited_persons, visited_keys

def convert_time_string(string):
    time_string = string
    lead_bought = datetime.datetime.strptime(time_string[0:time_string.rindex('.')],'%Y-%m-%dT%H:%M:%S')
    return lead_bought         

def process_persons(person_list,input_phone):

    # Convert to pipe delimited string
    person_string = '|'.join(map(str, person_list))

    # Request info
    url = "https://developer.goji.com/crmleadbuy/person?person_id=%s&require=ANY"

    cust_policy_url = "https://developer.goji.com/crmleadbuy/customer/%s/policy"
    cust_lead_url = "https://developer.goji.com/crmleadbuy/customer/%s/lead"
    lead_url = "https://developer.goji.com/crmleadbuy/lead/%s"

    headers = {
        'authorization': "Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIzIiwiZmlyc3RuYW1lIjoiQW5hbHl0aWNzIiwicm9sZXMiOlsiUk9MRV9BRE1JTiIsIlJPTEVfTUFOQUdFUiIsIlJPTEVfVVNFUiJdLCJleHAiOjIwMTg2NTg5ODQsImlhdCI6MTUxODY1ODk4NCwiZW1haWwiOiJzZXItYW5hbHl0aWNzQGdvamkuY29tIiwibGFzdG5hbWUiOiJTZXJ2aWNlIn0.UQ_000SdJGVwHReTKqrsgexjksj8kkFp8btPatRNjWFrtiYQM1xhNIV8_n-fVUnoQk2LAsIwQgfQPA9OSu_iTuEeBC07kP5rYPoCVm-yJlfGZSiNbSNNEH3B81s3ITY2bTG62WHI7tK9GQu41d2XP4gok-ah3y1x6HpQMU4mevRsURDqDYZDcHEanCTdpAKxdbjU7ZH2krE2HZMxEZbWH7-AE3L7QUghm7I_CPjyP84P8KFoJaupXU1oKZX60u_3zfsdf075HXoS_u_5-lqO3zjZyQXPLTOkchw0ExPqmWncApWWxDeNQuiG9aB3DzAbLlJbSC4gEmtRPgpmnMhRCg",
        'cache-control': "no-cache",
        'postman-token': "a7d6ede5-c693-f92d-e79e-130d68278abe"
        }

    response = requests.request("GET", url%(person_string), headers=headers)
    response_json = json.loads(response.text)

    # Place holders
    leads = []
    policies = []
    customers = []
    recent_purchase = 'N'
    active_policy = 'N'

    max_policy_inception_date = datetime.date(1900,1,1)
    max_policy_agent = 0

    max_lead_purchase_date = datetime.date(1900,1,1)
    max_lead_agent = 0

    phone_list = []

    for r in response_json:
        # Is there a customer?
        if 'customer' in r:
            # Get policy info
            cust_policy_response = requests.request("GET", cust_policy_url%(str(r['customer']['id'])), headers=headers)
            cust_policy_json = json.loads(cust_policy_response.text)

            customers.append(r['customer']['id'])
            
            for policy in cust_policy_json:
                policies.append(policy['id'])
                if policy['status'] in ('BOUND','CONFIRMED','ACTIVE'):
                    active_policy = 'Y'
                
                if datetime.datetime.strptime(policy['inception_date'],'%Y-%m-%d').date()  > max_policy_inception_date:
                    max_policy_inception_date = datetime.datetime.strptime(policy['inception_date'],'%Y-%m-%d').date()
                    try:
                        max_policy_agent = policy['credited_agent']['id']
                    except:
                        pass
            
            # Get Lead Info
            cust_lead_response = requests.request("GET", cust_lead_url%(str(r['customer']['id'])), headers=headers)
            cust_lead_json = json.loads(cust_lead_response.text)
            
            for lead in cust_lead_json: 
                leads.append(lead['id'])

                if (datetime.datetime.today().date() - convert_time_string(lead['acquisition_date']).date()).days < 120:
                    recent_purchase = 'Y'
                    
                if convert_time_string(lead['acquisition_date']).date()  > max_lead_purchase_date:
                    max_lead_purchase_date = convert_time_string(lead['acquisition_date']).date()
                    max_lead_agent = lead['owner']['id']
                    
        # Otherwise, it's a lead?
        elif 'lead' in r:
            lead_response = requests.request("GET", lead_url%(str(r['lead']['id'])), headers=headers)
            lead_json = json.loads(lead_response.text)
            
            leads.append(lead_json['id'])

            if (datetime.datetime.today().date() - convert_time_string(lead_json['acquisition_date']).date()).days < 120:
                recent_purchase = 'Y'
                
            if convert_time_string(lead_json['acquisition_date']).date()  > max_lead_purchase_date:
                max_lead_purchase_date = convert_time_string(lead_json['acquisition_date']).date()
                if 'owner' in lead_json:
                    max_lead_agent = lead_json['owner']['id']
                else:
                    max_lead_agent = 0

        # Add phones
        for phone in r['phones']:
            phone_clean = re.sub(r'\W+', '', phone['number'])
            if phone_clean not in phone_list:
                phone_list.append(phone_clean)         
            
    # Finalize Agent
    if len(policies) > 0:
        assiged_employee = max_policy_agent
    else:
        assiged_employee = max_lead_agent
        
    # Finalize reject decision
    if recent_purchase == 'Y' or active_policy == 'Y':
        reject = 'Y'
    else:
        reject = 'N'

    # Is it a new phone number
    new_phone = 'Y'
    for phone in phone_list:
        if phone in input_phone:
            new_phone = 'N'

    response_dict = {'reject':reject,'new_phone':new_phone,'persons':person_list,'customers':customers,'recent_purchase':recent_purchase, 'active_policy':active_policy,'assigned_employee':assiged_employee,'policies':policies,'leads':leads}
        
    return response_dict

def get_list_of_persons(keys,input_phone,arango_dict):

    # Connect to DB
    conn = Connection(  arangoURL=arango_dict['arango_url'],
                    username=arango_dict['arango_user'],
                    password=arango_dict['arango_password'])

    db = conn[arango_dict['arango_db_name']]

    keyCollection = db.collections["key"]
    personCollection = db.collections["person"]

    g = db.graphs["customer"]

    full_person_list = set()
    full_visited_keys = set()
    for key in keys:
        if key['_key'] not in full_visited_keys:
            try:
                existing_key = keyCollection[key['_key']]
                person_neighbors, visited_keys = find_persons_in_graph(key['_key'], arango_dict)
                full_person_list = full_person_list | person_neighbors
                full_visited_keys = full_visited_keys | visited_keys
            except Exception as e:
                pass

    full_person_list = list(full_person_list)
    
    if len(full_person_list) > 0:
        final_response = process_persons(full_person_list,input_phone)
    else:
        final_response = {'reject': 'N'}

    return final_response




