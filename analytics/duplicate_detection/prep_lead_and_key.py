# Given a lead JSON clean the data and prep it to compare to network
from clean_data.clean_first_name import clean_first_name
from clean_data.clean_last_name import clean_last_name
from clean_data.clean_phone import clean_phone
from clean_data.clean_address import clean_address
from clean_data.clean_dob import clean_dob
from clean_data.clean_email import clean_email

from create_keys.create_key import create_key

def prep_lead_and_key(lead_json):
    
    contact = lead_json['contact1']

    # Clean components
    first_name = clean_first_name(contact['first_name'])
    if contact['last_name'] != None:
        last_name = clean_last_name(contact['last_name'])
    else:
        last_name = None
    
    phones =[]
    if 'phones' in contact:
        for phone in contact['phones']:
            phones.append(clean_phone(phone['number']))
    
    if 'email' in contact:
        email = clean_email(contact['email'])
    else:
        email = None
    
    addresses = []
    if 'addresses' in contact:
        for address in contact['addresses']:
            if address['street1'] != None:
                street1 = address['street1']
                if 'street2' in address:
	                if address['street2'] != None:
	                    try:
	                        street2 = address['street2']
	                    except:
	                        street2 = ''
	                else:
	                    street2 = ''
                else:
	            	street2 = ''
                city = address['city']
                state = address['state']
                zip_code = address['zip_code'][:5]
              
                addr_clean, city, state, zip_code = clean_address(street1,street2,city,state,zip_code)
                
                addresses.append([addr_clean, city, state, zip_code])
    
    if 'date_of_birth' in contact:
        if contact['date_of_birth'] != None:
            dob = clean_dob(contact['date_of_birth'])
        else:
            dob = None
    else:
        dob = None
            
    
    # Create Keys
    keys = []
    
    if first_name != None and email != None:
        keys.append(create_key({'first_name':first_name,'email':email}))
    
    if dob != None and email != None:
        keys.append(create_key({'dob':dob,'email':email}))
    
    for phone in phones:
        if first_name != None and last_name != None and phone != None:
            keys.append(create_key({'first_name':first_name,'last_name':last_name,'phone':phone}))
        if phone != None and email != None:
            keys.append(create_key({'phone':phone,'email':email}))
        if dob != None:
            keys.append(create_key({'phone':phone,'dob':dob}))
    
    for address in addresses:
        keys.append(create_key({'first_name':first_name,'addr_clean':addr_clean,'city':city,'state':state}))
        if dob != None:
            keys.append(create_key({'dob':dob,'addr_clean':addr_clean,'city':city,'state':state}))
        for phone in phones:
            keys.append(create_key({'first_name':first_name,'phone':phone,'zip_code':zip_code}))

    return keys



