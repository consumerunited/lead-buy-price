# Function for cleaning first name

def clean_first_name(first_name):
    return first_name.upper().replace(" ", "")