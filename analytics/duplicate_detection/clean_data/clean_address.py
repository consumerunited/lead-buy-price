# Function for cleaning address
import usaddress

def standardizeStreetType(varName):
    varName=varName.replace('STATE','ST')
    varName=varName.replace('HIGHWAY','HWY')
    varName=varName.replace('ROUTE','RTE')
    varName=varName.replace('ROAD','RD')
    varName=varName.replace('COUNTY','CNTY')
    varName=varName.replace('STREET','ST')
    varName=varName.replace('DRIVE','DR')
    varName=varName.replace('CIRCLE','CIR')
    varName=varName.replace('COURT','CT')
    varName=varName.replace('LANE','LN')
    varName=varName.replace('AVENUE','AVE')
    varName=varName.replace('TURNPIKE','TPKE')
    varName=varName.replace('ALLEY','ALY')
    varName=varName.replace('ANNEX','ANX')
    varName=varName.replace('ARCADE','ARC')
    varName=varName.replace('BLUFF','BLF')
    varName=varName.replace('BOULEVARD','BLVD')
    varName=varName.replace('BRIDGE','BRG')
    varName=varName.replace('TERRACE','TER')
    varName=varName.replace('HEIGHTS','HTS')
    varName=varName.replace('PLAZA','PLZ')
    varName=varName.replace('TERRACE','TER')
    varName=varName.replace('PARKWAY','PKWY')
    varName=varName.replace('GARDENS','GDNS')
    varName=varName.replace('GARDEN','GDNS')
    varName=varName.replace('RIDGE','RDG')
    varName=varName.replace('MEADOWS','MDWS')
    varName=varName.replace('MEADOW','MDWS')
    varName=varName.replace('SQUARE','SQ')
    varName=varName.replace('VILLAGE','VLG')
    varName=varName.replace('PLACE','PL')
    varName=varName.replace('TRAIL','TRL')
    varName=varName.replace('TRACE','TRCE')
    varName=varName.replace('AVEUNE','AVE')
    varName=varName.replace('PARKWAY','PKWY')
    varName=varName.replace('RIDGE','RDG')
    varName=varName.replace('STEET','ST')
    return varName

def clean_address(street1,street2,city,state,zip_code):
    
    if street2 == '' or street2 == None: 
        concat_addr = street1 + " " + city + ", " + state
    else:
        concat_addr = street1 + " " + street2 + " " + city + ", " + state

    addr_parsed_list = usaddress.parse(concat_addr.upper())
    
    addr_parsed= {}
    
    for adr_type in addr_parsed_list:
        addr_parsed[adr_type[1]] = adr_type[0]
        

    addr_clean = ''
    try:
        addr_clean = addr_clean + addr_parsed['AddressNumber'].upper()
    except:
        pass
    try:
        addr_clean = addr_clean + addr_parsed['StreetNamePreDirectional'].upper()
    except:
        pass
    try:
        addr_clean = addr_clean + addr_parsed['StreetName'].upper()
    except:
        pass
    try:
        addr_parsed['StreetNamePostType'] = standardizeStreetType(addr_parsed['StreetNamePostType'])
        addr_clean = addr_clean + addr_parsed['StreetNamePostType'].upper()
    except:
        pass
    try:
        addr_clean = addr_clean + addr_parsed['OccupancyType'].upper()
    except:
        pass
    try:
        addr_clean = addr_clean + addr_parsed['OccupancyIdentifier'].upper()
    except:
        pass  
    
    return addr_clean.replace(' ','').upper(), city.upper(), state.upper(), zip_code.upper()