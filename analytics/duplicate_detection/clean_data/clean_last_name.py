# Function for cleaning last name

def clean_last_name(last_name):
    return last_name.upper().replace(" ", "")