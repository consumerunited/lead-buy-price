# Function for creating key
import hashlib

def hash_key(key):
    return hashlib.sha256(key).hexdigest()

# Take dictionary of components in key and values and create key dict
def create_key(components):

	key_str = ''
	type_str = ''
	loop = 0

	for component in components:
	    if loop != 0:
	        key_str += "_"
	        type_str += "_"
	    key_str += components[component]
	    type_str += component
	    
	    loop += 1
	    
	hashed_key = hash_key(key_str)

	components["type"] = type_str
	components["_key"] = hashed_key

	return components