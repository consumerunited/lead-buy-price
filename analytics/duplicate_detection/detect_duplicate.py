import logging
import os
import sys
import json as jsn
import re

from flask import request, jsonify
from flask_restplus import Resource
from flask import current_app as app
from analytics.serializers.lead import lead
from analytics.serializers.duplicate_response import duplicate_response
from analytics.restplus import analytics

# Parse data out of lead
from analytics.get_price import parse_lead_request

# Scoring models
from prep_lead_and_key import prep_lead_and_key

log = logging.getLogger(__name__)

ns = analytics.namespace('detect_duplicate')

# Function for traversing graph
from traverse_graph import get_list_of_persons


@ns.route('/')
class detect_duplicate(Resource):

    @analytics.expect(lead)
    @analytics.marshal_with(duplicate_response)
    def post(self):
    	"""
        Given a lead return a list of persons associated with it
        """       
        lead_id = request.json['id']
        keys = prep_lead_and_key(request.json)

        # Arango Connection Stuff
        arango_dict = { 'arango_url':app.config['ARANGO_URL'],
                        'arango_user':app.config['ARANGO_USER'],
                        'arango_password':app.config['ARANGO_PASSWORD'],
                        'arango_db_name':app.config['ARANGO_DB_NAME']
                     }

        # Phones
        phone_list = []
        for phone in request.json['contact1']['phones']:
            phone_clean = re.sub(r'\W+', '', phone['number'])
            if phone_clean not in phone_list:
                phone_list.append(phone_clean)   

        # Get linked persons
        final_response = get_list_of_persons(keys,phone_list,arango_dict)

        # Add some info
        final_response['id'] = lead_id

    	return final_response,201
