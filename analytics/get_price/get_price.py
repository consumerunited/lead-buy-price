import logging
import os
import sys
import json as jsn

from flask import request, jsonify
from flask_restplus import Resource
from analytics.serializers.lead import lead
from analytics.serializers.price_response import price_response
from analytics.restplus import analytics

# Pricing Models
from models.lead_price.ltv import get_ltv
from models.lead_price.conversion import get_conversion
from models.lead_price.demand import get_demand
from models.lead_price.market import get_market
from models.lead_price.price import generate_price

# Parse data out of lead
from analytics.get_price import parse_lead_request

log = logging.getLogger(__name__)

ns = analytics.namespace('get_price')

@ns.route('/')
class get_price(Resource):

    @analytics.expect(lead)
    @analytics.marshal_with(price_response, code = 201)
    def post(self):
        """
        Given a lead return a price
        """        
        data = request.json

        #file = open(os.environ['APP_ROOT'] + "/requests/" + str(data['id']) + ".json","w")
        #file.write(jsn.dumps(data)) 
        #file.close()

        # Parse data
        parsed_lead_data, zip_data, conversion_data, jornaya_dict = parse_lead_request.transform_lead(request.json)
        lead_id = request.json['id']

        # How much LTV from lead?
        ltv, ltv_model_version = get_ltv.get_ltv(parsed_lead_data, zip_data)

        # How Likely am I to convert this lead?
        conv, st_conv, lt_conv, conv_model_version = get_conversion.get_conversion(parsed_lead_data,zip_data,conversion_data, jornaya_dict)

        # What does our demand look like?
    	demand_factor, demand_model_version = get_demand.get_demand()

    	# What does the market look like?
    	market_factor, market_model_version = get_market.get_market()

    	# Using factors above spit out a price
    	base_price, bid_price, price_model_version, margin_factor = generate_price.generate_price(ltv, conv, market_factor, demand_factor, campaign_id=parsed_lead_data['campaign_id'],vehicles=parsed_lead_data['vehicles'])

    	response_dict = {	"id": lead_id,
							"ltv": ltv,
							"conversion":	{	"conversion": round(conv,5),
												"short_term_conversion": round(st_conv,5),
												"long_term_conversion": round(lt_conv,5)
											},
							"demand": round(demand_factor,5),
							"market": round(market_factor,5),
							"price": {	"bid": round(bid_price,2),
										"base": round(base_price,2)
									 },
							"version": {	"ltv": ltv_model_version,
											"conversion": conv_model_version,
											"demand": demand_model_version,
											"market": market_model_version,
											"price": price_model_version
										},
                            "margin": round(margin_factor,5)}
                            
        return response_dict, 201


