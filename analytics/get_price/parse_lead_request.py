
# Parse data we need from lead
from datetime import date
import numpy as np
import datetime
import pandas as pd
import os
from cache.cache import cache
from db_model.db import db
from db_model.support_data import support_data
from db_model.support_data_values import support_data_values
from db_model.zip_data import zip_data as zip_data_db

# Helper Function for calculating data
def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
    
# Create data frame for zip code data
zip_demo_data = pd.read_csv(os.environ['APP_ROOT'] + "/models/normalizers/zip_code_normalizers/zip_code_demographics.csv",dtype={'zip':object})

# Helper Function for getting support data vlues
def convert_support_data_value_to_dict(support_data_name):
    support_data_id = db.session.query(support_data).filter(support_data.name == support_data_name).first().support_data_id
    support_data_values_list = db.session.query(support_data_values).filter(support_data_values.support_data_id == support_data_id).all()
    support_dict = {}
    for value in support_data_values_list:
        support_dict[value.key] = value.value
    return support_dict

# Overall Conversion Data
@cache.memoize(timeout=3600)
def get_overall_conversion():
    return convert_support_data_value_to_dict('overall_conversion')

# Campaign Conversion Data
@cache.memoize(timeout=3600)
def get_campaign_conversion():
    return convert_support_data_value_to_dict('campaign_conversion')

# Source Conversion Data
@cache.memoize(timeout=3600)
def get_source_conversion():
    return convert_support_data_value_to_dict('source_conversion')

# State Conversion Data
@cache.memoize(timeout=3600)
def get_state_conversion():
    return convert_support_data_value_to_dict('state_conversion')

# Zip Short Conversion Data
@cache.memoize(timeout=3600)
def get_zip_short_conversion():
    return convert_support_data_value_to_dict('zip_short_conversion')

# Zip Data
def get_zip_data_dict(zip_code):
    zip_data = db.session.query(zip_data_db).filter(zip_data_db.zip == zip_code).first()

    zip_data_dict = {}
    zip_data_dict['median_earning'] = int(zip_data.median_earning)
    zip_data_dict['overall_bach_or_hgher'] = float(zip_data.overall_bach_or_hgher)
    zip_data_dict['total_pop'] = int(zip_data.total_pop)
    zip_data_dict['cmtg_public_per'] = float(zip_data.cmtg_public_per)
    zip_data_dict['rural_pop_per'] = float(zip_data.rural_pop_per)
    zip_data_dict['urban_pop_per'] = float(zip_data.urban_pop_per)
    zip_data_dict['mrd_cpl_per'] = float(zip_data.mrd_cpl_per)
    zip_data_dict['oo_value_median'] = int(zip_data.oo_value_median)
    zip_data_dict['hsehld_utility_gas_per'] = float(zip_data.hsehld_utility_gas_per)
    zip_data_dict['owner_occupied_per'] = float(zip_data.owner_occupied_per)
    zip_data_dict['unit_median_rm'] = float(zip_data.unit_median_rm)
    zip_data_dict['per_capita_income'] = int(zip_data.per_capita_income)
    zip_data_dict['median_age'] = float(zip_data.median_age)
    zip_data_dict['over_18_pop_per'] = float(zip_data.over_18_pop_per)
    zip_data_dict['over_62_pop_per'] = float(zip_data.over_62_pop_per)
    zip_data_dict['overall_hgh_or_hgher'] = float(zip_data.overall_hgh_or_hgher)
    zip_data_dict['cmtg_per'] = float(zip_data.cmtg_per)
    zip_data_dict['cmtg_car_alone_per'] = float(zip_data.cmtg_car_alone_per)
    zip_data_dict['mean_commute_time'] = float(zip_data.mean_commute_time)
    zip_data_dict['households_w_snap_per'] = float(zip_data.households_w_snap_per)
    zip_data_dict['all_people_blw_pvty_level'] = float(zip_data.all_people_blw_pvty_level)
    zip_data_dict['homeowner_vacancy_rate'] = float(zip_data.homeowner_vacancy_rate)
    zip_data_dict['renter_occupied_per'] = float(zip_data.renter_occupied_per)
    zip_data_dict['hsehld_1_vehicle_per'] = float(zip_data.hsehld_1_vehicle_per)
    zip_data_dict['hsehld_2_vehicles_per'] = float(zip_data.hsehld_2_vehicles_per)
    zip_data_dict['hsehld_3_or_more_vehicle_per'] = float(zip_data.hsehld_3_or_more_vehicle_per)
    zip_data_dict['hsehld_electricity_per'] = float(zip_data.hsehld_electricity_per)
    zip_data_dict['oo_w_mrtg_per'] = float(zip_data.oo_w_mrtg_per)
    zip_data_dict['fam_per'] = float(zip_data.fam_per)
    zip_data_dict['nonfam_hsehlds_per'] = float(zip_data.nonfam_hsehlds_per)
    zip_data_dict['urban_inside_area_pop_per'] = float(zip_data.urban_inside_area_pop_per)
    zip_data_dict['urban_cluster_pop_per'] = float(zip_data.urban_cluster_pop_per)

    return zip_data_dict


def transform_lead(parsed_json):
    
    # Which person is the primary driver?
    relation_index = 0
    homeowner_index = 0
    count = 0
    
    for person in parsed_json['drivers']:
        # If the person has a homeowner indicator they are the first person
        if 'home_ownership' in person:
            homeowner_index = count 
        # If the person's relationship is insured then they're the primary driver
        if 'relation' in person:
            if person['relation'] == 'INSURED':
                relation_index = count 
        count += 1
        
    if relation_index != 0:
        primary_driver = relation_index
    elif homeowner_index != 0:
        primary_driver = homeowner_index
    else:
        primary_driver = 0
            
    # Get Prior Carrier Information
    if 'prior_policy' in parsed_json:
        if 'carrier' in parsed_json['prior_policy']:
            prior_carrier = parsed_json['prior_policy']['carrier']['code']
        else:
            prior_carrier = None
        if 'months_with_prior_carrier' in parsed_json['prior_policy']:
            months_w_prior_carrier = parsed_json['prior_policy']['months_with_prior_carrier']
        else:
            months_w_prior_carrier = None
        if 'liability_limit' in parsed_json['prior_policy']:
            liability_limit = parsed_json['prior_policy']['liability_limit']
        else:
            liability_limit = None
    else:
        prior_carrier = None
        months_w_prior_carrier = None
        liability_limit = None

    # Get Homeownership
    if 'home_ownership' in parsed_json['drivers'][primary_driver]:
        homeownership = parsed_json['drivers'][primary_driver]['home_ownership']
    else:
        if 'home_ownership' in parsed_json['contact1']:
            homeownership = parsed_json['contact1']['home_ownership']
        else:
            homeownership = None

    # Get Gender
    if 'gender' in parsed_json['drivers'][primary_driver]:
        gender = parsed_json['drivers'][primary_driver]['gender']
    else:
        gender = None
        
    # Get Marital Status
    if 'marital_status' in parsed_json['drivers'][primary_driver]:
        marital_status = parsed_json['drivers'][primary_driver]['marital_status']
    else:
        marital_status = None

    # Get Age
    if 'date_of_birth' in parsed_json['drivers'][primary_driver]:
        dob  = datetime.datetime.strptime(parsed_json['drivers'][primary_driver]['date_of_birth'],'%Y-%m-%d').date()
        age = calculate_age(dob)
    else:
        age = None
        
    # Get Education
    if 'education'  in parsed_json['drivers'][primary_driver]:
        education = parsed_json['drivers'][primary_driver]['education']
    else:
        education = None

    # Get Credit
    if 'credit'  in parsed_json['drivers'][primary_driver]:
        credit = parsed_json['drivers'][primary_driver]['credit']
    else:
        credit = None
    
    # Get Email
    if 'email'  in parsed_json['drivers'][primary_driver]:
        email = parsed_json['drivers'][primary_driver]['email']
    else:
        email = None
    
    # How many drivers
    drivers = len(parsed_json['drivers'])
        
    # Did i identify the model?
    if 'year' in parsed_json['vehicles'][0]:
        # What year is the car?
        vehicle_age = date.today().year - parsed_json['vehicles'][0]['year']
    else:
        try:
            vehicle_age = date.today().year - int(parsed_json['vehicles'][0]['description'][:4])
        except:
            vehicle_age = None
        
    # Vehicle Use
    if 'use' in parsed_json['vehicles'][0]:
        vehicle_use = parsed_json['vehicles'][0]['use']
    else:
        vehicle_use= None
        
    # How many vehicles
    vehicles = len(parsed_json['vehicles'])

    # What zip code?
    if 'contact1' in parsed_json:
        if 'addresses' in parsed_json['contact1']:
            address = parsed_json['contact1']['addresses'][0]
            if 'zip_code' in address:
                zip_code = address['zip_code']
                if len(zip_code) > 5:
                    zip_code = zip_code[:5]
            else:
                zip_code = None
        else:
            zip_code = None
    else:
        zip_code = None

    # Get Campaign
    if 'campaign' in parsed_json:
        campaign_id = parsed_json['campaign']['id']
    else:
        campaign_id = -1

    # Get Zip Data
    try:
        zip_data = get_zip_data_dict(zip_code)
        cmtg_public_per = zip_data['cmtg_public_per']
        rural_pop_per = zip_data['rural_pop_per']
        unit_median_rm = zip_data['unit_median_rm']
        overall_bach_or_hgher = zip_data['overall_bach_or_hgher']
        urban_pop_per = zip_data['urban_pop_per']
        total_pop = zip_data['total_pop']
        oo_value_median = zip_data['oo_value_median']
        median_earning = zip_data['median_earning']
        per_capita_income = zip_data['per_capita_income']
        hsehld_utility_gas_per = zip_data['hsehld_utility_gas_per']
        mrd_cpl_per = zip_data['mrd_cpl_per']
        owner_occupied_per = zip_data['owner_occupied_per']
        median_age = zip_data['median_age']
        over_18_pop_per = zip_data['over_18_pop_per']
        over_62_pop_per = zip_data['over_62_pop_per']
        overall_hgh_or_hgher = zip_data['overall_hgh_or_hgher']
        cmtg_per = zip_data['cmtg_per']
        cmtg_car_alone_per = zip_data['cmtg_car_alone_per']
        mean_commute_time = zip_data['mean_commute_time']
        households_w_snap_per = zip_data['households_w_snap_per']
        all_people_blw_pvty_level = zip_data['all_people_blw_pvty_level']
        homeowner_vacancy_rate = zip_data['homeowner_vacancy_rate']
        renter_occupied_per = zip_data['renter_occupied_per']
        hsehld_1_vehicle_per = zip_data['hsehld_1_vehicle_per']
        hsehld_2_vehicles_per = zip_data['hsehld_2_vehicles_per']
        hsehld_3_or_more_vehicle_per = zip_data['hsehld_3_or_more_vehicle_per']
        hsehld_electricity_per = zip_data['hsehld_electricity_per']
        oo_w_mrtg_per = zip_data['oo_w_mrtg_per']
        fam_per = zip_data['fam_per']
        nonfam_hsehlds_per = zip_data['nonfam_hsehlds_per']
        urban_inside_area_pop_per = zip_data['urban_inside_area_pop_per']
        urban_cluster_pop_per = zip_data['urban_cluster_pop_per']
    except: 
        cmtg_public_per = np.nan
        rural_pop_per = np.nan
        unit_median_rm = np.nan
        overall_bach_or_hgher = np.nan
        urban_pop_per = np.nan
        total_pop = np.nan
        oo_value_median = np.nan
        median_earning = np.nan
        per_capita_income = np.nan
        hsehld_utility_gas_per = np.nan
        mrd_cpl_per = np.nan
        owner_occupied_per = np.nan
        median_age = np.nan
        over_18_pop_per = np.nan
        over_62_pop_per = np.nan
        overall_hgh_or_hgher = np.nan
        cmtg_per = np.nan
        cmtg_car_alone_per = np.nan
        mean_commute_time = np.nan
        households_w_snap_per = np.nan
        all_people_blw_pvty_level = np.nan
        homeowner_vacancy_rate = np.nan
        renter_occupied_per = np.nan
        hsehld_1_vehicle_per = np.nan
        hsehld_2_vehicles_per = np.nan
        hsehld_3_or_more_vehicle_per = np.nan
        hsehld_electricity_per = np.nan
        oo_w_mrtg_per = np.nan
        fam_per = np.nan
        nonfam_hsehlds_per = np.nan
        urban_inside_area_pop_per = np.nan
        urban_cluster_pop_per = np.nan


    # Conversion support data
    overall_conversion = get_overall_conversion()
    campaign_conversion = get_campaign_conversion()
    source_conversion = get_source_conversion()
    state_conversion = get_state_conversion()
    zip_short_conversion = get_zip_short_conversion()

    # Create Conversion Dictionary
    conversion_dict = {}
    conversion_dict['overall_conversion'] = overall_conversion['conversion']

    try:
        conversion_dict['campaign_conversion'] = campaign_conversion[str(parsed_json['campaign']['id'])]
    except:
        conversion_dict['campaign_conversion'] = conversion_dict['overall_conversion']

    try:
        conversion_dict['source_conversion'] = source_conversion[str(parsed_json['campaign']['id']) + "_" + parsed_json['partner_source_code']]
    except:
        conversion_dict['source_conversion'] = conversion_dict['campaign_conversion']

    try:
        conversion_dict['state_conversion'] = state_conversion[parsed_json['contact1']['addresses'][0]['state']]
    except:
        conversion_dict['state_conversion'] = conversion_dict['overall_conversion']

    try:
        conversion_dict['zip_short_conversion'] = zip_short_conversion[parsed_json['contact1']['addresses'][0]['zip_code'][:3]]
    except:
        conversion_dict['zip_short_conversion'] = conversion_dict['state_conversion']


    # Jornaya Data
    jornaya_dict = {}

    try:
        jornaya_dict['jornaya_age'] = parsed_json['jornaya']['age']
    except:
        jornaya_dict['jornaya_age'] = None

    try:
        jornaya_dict['duration'] = parsed_json['jornaya']['duration']
    except:
        jornaya_dict['duration'] = None

    try:
        jornaya_dict['total_entities'] = parsed_json['jornaya']['total_entities']
    except:
        jornaya_dict['total_entities'] = None

    try:
        jornaya_dict['total_hops'] = parsed_json['jornaya']['total_hops']
    except:
        jornaya_dict['total_hops'] = None

    return  {   'lead_id':parsed_json['id'],
                'campaign_id':campaign_id,
                'prior_carrier':prior_carrier,
                'months_with_prior_carrier':months_w_prior_carrier,
                'homeownership':homeownership,
                'gender':gender,
                'marital_status':marital_status,
                'age':age,
                'education':education,
                'credit':credit,
                'email':email,
                'vehicle_age':vehicle_age,
                'vehicle_use':vehicle_use,
                'vehicles':vehicles,
                'drivers':drivers,
                'liability_limit':liability_limit,
                'zip_code':zip_code},{
                'cmtg_public_per':cmtg_public_per,
                'rural_pop_per':rural_pop_per,
                'cmtg_public_per':cmtg_public_per,
                'unit_median_rm':unit_median_rm,
                'overall_bach_or_hgher':overall_bach_or_hgher,
                'urban_pop_per':urban_pop_per,
                'total_pop':total_pop,
                'oo_value_median':oo_value_median,
                'median_earning':median_earning,
                'per_capita_income':per_capita_income,
                'hsehld_utility_gas_per':hsehld_utility_gas_per,
                'mrd_cpl_per':mrd_cpl_per,
                'owner_occupied_per':owner_occupied_per,
                'median_age':median_age,
                'over_18_pop_per':over_18_pop_per,
                'over_62_pop_per':over_62_pop_per,
                'overall_hgh_or_hgher':overall_hgh_or_hgher,
                'cmtg_per':cmtg_per,
                'cmtg_car_alone_per':cmtg_car_alone_per,
                'mean_commute_time':mean_commute_time,
                'households_w_snap_per':households_w_snap_per,
                'all_people_blw_pvty_level':all_people_blw_pvty_level,
                'homeowner_vacancy_rate':homeowner_vacancy_rate,
                'renter_occupied_per':renter_occupied_per,
                'hsehld_1_vehicle_per':hsehld_1_vehicle_per,
                'hsehld_2_vehicles_per':hsehld_2_vehicles_per,
                'hsehld_3_or_more_vehicle_per':hsehld_3_or_more_vehicle_per,
                'hsehld_electricity_per':hsehld_electricity_per,
                'oo_w_mrtg_per':oo_w_mrtg_per,
                'fam_per':fam_per,
                'nonfam_hsehlds_per':nonfam_hsehlds_per,
                'urban_inside_area_pop_per':urban_inside_area_pop_per,
                'urban_cluster_pop_per':urban_cluster_pop_per
                },conversion_dict, jornaya_dict




