import logging
import traceback

from flask_restplus import Api

log = logging.getLogger(__name__)

analytics = Api( version='3.0', 
	   title='Goji Analytics API',
       description='A simple API to allow Goji to access models developed by analytics')


@analytics.errorhandler
def default_error_handler(e):
	return {'message': str(e)}, getattr(e, 'code', 500)
 
