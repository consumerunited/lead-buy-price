## Normalizer for primary driver credit

def goji_norm_primary_driver_credit(credit):

	if credit != None:
		credit = credit.upper()

	credit_high = 0
	credit_medium = 0
	credit_low =0
	credit_none = 0

	if credit != None:
		if credit in ('EXCELLENT','VERY_GOOD'):
			credit_high = 1
		elif credit in ('FAIR','GOOD'):
			credit_medium = 1
		elif credit in ('BELOW_FAIR', 'POOR','VERY_POOR'):
			credit_low = 1
		else:
			credit_other = 1
	else:
		credit_none = 1

	return credit_high, credit_medium, credit_low, credit_none