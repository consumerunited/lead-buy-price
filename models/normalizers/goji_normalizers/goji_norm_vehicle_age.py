# Function for Normalizing the age of a vehicle
# Function assumes non-linear model, if using a linear model makes sense to break this out in chunks

import numpy as np

def goji_norm_vehicle_age(age):
	if age == None or np.isnan(age):
		age = 8

	if age > 25:
		age = 25

	norm_age = (age-0.)/(25.-0.)

	return norm_age
