# Function for Normalizing our number of drivers

def goji_norm_count_drivers(drivers):

	if drivers is None or drivers == 0:
		drivers = 1

	if drivers > 4:
		drivers = 4

	drivers_norm = (drivers - 1.)/(4.-1.)

	return drivers_norm