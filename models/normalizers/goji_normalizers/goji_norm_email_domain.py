## Normalize email domain (john.smith@goji."COM")
## Certian domains regardless of age and other factors seem to move LTV
import re

def goji_norm_email_domain(email):

	if email != None:
		domain = re.search("\.[\w.]+",re.search("@.*", email).group()).group()[1:]
		domain = domain.upper()
	else:
		domain = None

	high_domain = 0
	low_domain = 0

	if domain != None:
		if domain in ('NET','US','ORG'):
			high_domain = 1
		elif domain in ('EDU','CON'):
			low_domain = 1

	return high_domain, low_domain