# Function for normalizing vehicle use

def goji_norm_vehicle_use(use):

	use_other = 0
	use_business = 0
	use_pleasure = 0
	use_work = 0
	
	if use != None:
		use = use.upper()
		if use == 'BUSINESS':
			use_business = 1
		elif use == 'PLEASURE':
			use_pleasure = 1
		elif use == 'COMM_WORK':
			use_work = 1
		else:
			use_other = 1

	return use_other, use_business, use_pleasure, use_work