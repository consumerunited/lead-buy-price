## Function for normalizin home ownership

def goji_norm_home_ownership(status):

	if status != None:
		status = status.upper()

	resident_own = 0
	resident_rent = 0
	resident_other = 0
	resident_unknown = 0

	if status != None:
		if status in('HOME_OWNED','CONDO_OWNED'):
			resident_own = 1
		elif status in('RENTAL','APARTMENT'):
			resident_rent = 1
		elif status in('OTHER','WITH_PARENT','MOBILE_HOME'):
			resident_other = 1
		else:
			resident_unknown = 1
	else:
		resident_unknown = 1

	return resident_own, resident_rent, resident_other, resident_unknown
	