# Function for normalizing marital status

def goji_norm_primary_driver_marital_status(status):

	marital_divorced = 0
	marital_married = 0
	marital_other = 0
	marital_single = 0
	marital_widowed = 0
	
	if status != None:
		status = status.upper()

		if status in('DIVORCED','SEPARATED'):
			marital_divorced = 1
		elif status == 'MARRIED':
			marital_married = 1
		elif status == 'SINGLE':
			marital_single = 1
		elif status == 'WIDOWED':
			marital_widowed = 1
		else:
			marital_other = 1

	return marital_divorced, marital_married, marital_other, marital_single, marital_widowed