# Some email providers have on average better LTV than others
# Some email providers have materially worse LTV than others
# This seems to be independent of age and other strongly correlated factors
import re

def goji_norm_email_provider(email):

	if email != None:
		provider  = re.search("[\w.]+\.",re.search("@.*", email).group()).group()[:-1]
		provider = provider.upper()
	else:
		provider = None

	high_provider = 0
	low_provider = 0

	if provider != None:
		if provider in ('OPTONLINE','VERIZON','FRONTIER','SBCGLOBAL','COMCAST','COX','MSN','JUNO','WINDSTREAM'):
			high_provider = 1
		elif provider in ('YAHOO','GMAIL','ROCKETMAIL','YMAIL'):
			low_provider = 1

	return high_provider, low_provider