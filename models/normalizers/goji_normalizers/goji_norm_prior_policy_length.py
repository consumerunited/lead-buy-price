## Function for normalizing current insurance length

def goji_norm_prior_policy_length(length):

	if length != None:
		length = 0

	prior_none = 0
	prior_less_than_one = 0
	prior_one_to_two = 0
	prior_more_than_two = 0

	if length == None or length <= 0:
		prior_none = 1
	elif length >= 1 and length <= 12:
		prior_less_than_one = 1
	elif length > 12 and length <= 24:
		prior_one_to_two = 1
	else:
		prior_more_than_two = 1

	return prior_none, prior_less_than_one, prior_one_to_two, prior_more_than_two
