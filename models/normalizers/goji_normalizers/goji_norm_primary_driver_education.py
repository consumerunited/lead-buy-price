# Function normalizing education

def goji_norm_primary_driver_education(education):

	education_associates = 0
	education_bachelors = 0
	education_high = 0
	education_masters = 0
	education_no_high = 0
	education_phd = 0
	education_some_college = 0
	education_other = 0

	if education != None:
		education = education.upper().strip()
		
		if education == 'ASSOCIATES': 
			education_associates = 1
		elif education == 'BACHELORS':
			education_bachelors = 1
		elif education in('GED','HIGH_SCHOOL'):
			education_high = 1
		elif education in('MASTERS'):
			education_masters = 1
		elif education == 'NO HIGH SCHOOL DIPLOMA':
			education_no_high = 1
		elif education in('LAW','MEDICAL','DOCTORATE'):
			education_phd = 1
		elif education in('IN_COLLEGE','SOME_COLLGE'):
			education_some_college = 1
		else:
			education_other = 1

	return education_associates, education_bachelors, education_high, education_masters, education_no_high, education_phd, education_some_college, education_other