## Function for normalizing current liability limits

def goji_norm_prior_policy_limit(limit):

	if limit != None:
		limit = limit.upper()

	limit_none = 0
	limit_high = 0
	limit_low = 0

	if limit != None:
		limit = limit.upper()

		if limit == None or limit == '':
			limit_none = 1
		elif limit in('IL100_300','IL250_500','IL300_300','IL1000_1000','IL500_500'):
			limit_high = 1
		else:
			limit_low = 1
	else:
		limit_none = 1

	return limit_none, limit_high, limit_low