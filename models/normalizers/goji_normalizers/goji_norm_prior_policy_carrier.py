# Function for standardizing carriers to be addressed by model

def goji_norm_prior_policy_carrier(carrier):

	missing_prior = 0
	no_prior = 0
	pref_prior = 0
	std_prior = 0
	nstd_prior = 0
	unsure_prior = 0
	
	if carrier != None:
		carrier = carrier.upper()

		if carrier == None or carrier == '':
			no_prior = 1
		elif carrier in ('HARTFD','AMICA','NATION','LIBMTL','ALLSTE','PRUDEN','KEMPER','PEMCO','EMC','FARMER','TRAVRS','METLFE','METROP','SAFECO'):
			pref_prior = 1
		elif carrier in ('DIRGEN','SENTRY','AAA','USAA','FARMBU','AIG','GRANGE','GEICO','GNRCAS','PATRIO','NATGEN','SHELTR','PROGSS','UNITRN','ESURAN','21CENT','FARMBU','ERIE','AMFAMI','HLLMRK','HANOVR','PLMRCK','STDMTL','CNTINS','ALLIED','STFARM'):
			std_prior = 1
		elif carrier in ('UNSURE','OTHER'):
			unsure_prior = 1
		else:
			nstd_prior = 1
	else:
		missing_prior = 1

	return missing_prior, no_prior, pref_prior, std_prior, nstd_prior, unsure_prior
