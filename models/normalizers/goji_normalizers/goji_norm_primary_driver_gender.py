# Normalize the drivers gender

def goji_norm_primary_driver_gender(gender):

	gender_male = 0
	gender_female = 0
	gender_other = 0

	if gender != None:
		gender = gender.upper()

		if gender == 'M':
			gender_male = 1
		elif gender == 'F':
			gender_female = 1
		else:
			gender_other = 0

	return gender_male, gender_female, gender_other