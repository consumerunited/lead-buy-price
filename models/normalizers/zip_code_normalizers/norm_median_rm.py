# Normalizer for number of rooms
import norm_min_max
import numpy as np


def norm_median_rm(per):

	if per == None or np.isnan(per):
		per = 5.6366

	return norm_min_max.norm_min_max(per,0.,9.)