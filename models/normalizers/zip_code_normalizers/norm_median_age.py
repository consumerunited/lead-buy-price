# Normalize the median age in a zip code
import norm_min_max
import numpy as np

def norm_median_age(age):

	if age == None or np.isnan(age):
		age = 35.5

	return norm_min_max.norm_min_max(age,20.,70.)