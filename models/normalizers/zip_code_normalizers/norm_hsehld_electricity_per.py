# Normalizer for % of households with electric heat
import norm_min_max
import numpy as np

def norm_hsehld_electricity_per(per):

	hsehld_electricity_per = round(per,1)
	if np.isnan(hsehld_electricity_per):
		hsehld_electricity_per = -1
	return hsehld_electricity_per