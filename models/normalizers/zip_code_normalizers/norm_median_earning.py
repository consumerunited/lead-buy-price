# Normalizer for median earning
import norm_min_max
import numpy as np

def norm_median_earning(earn):

	if earn == None or np.isnan(earn):
		earn = 30057.0369

	return norm_min_max.norm_min_max(earn,0.,115893.)