# Normalizer for percent of oo house with a mortgage
import norm_min_max
import numpy as np

def norm_oo_w_mrtg_per(per):

	if per == None or np.isnan(per):
		per = 0.64984866

	return norm_min_max.norm_min_max(per,0.,1.)
	