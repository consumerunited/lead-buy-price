# Normalizer for % of households with 3 vehicls
import norm_min_max
import numpy as np

def norm_hsehld_3_or_more_vehicle_per(per):

	if per == None or np.isnan(per):
		per = 0.19910660

	return norm_min_max.norm_min_max(per,0.,1.)
	