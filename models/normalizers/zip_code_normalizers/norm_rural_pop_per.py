# Normalizer for rural pop per
import numpy as np

def norm_rural_pop_per(per):

    rural_pop_per = round(per,1)
    if np.isnan(rural_pop_per):
        rural_pop_per = -1

    return rural_pop_per
	