# Normalizer for % of households with 2 vehicls
import norm_min_max
import numpy as np

def norm_hsehld_2_vehicle_per(per):

	if per == None or np.isnan(per):
		per = 0.37836018

	return norm_min_max.norm_min_max(per,0.,1.)
	