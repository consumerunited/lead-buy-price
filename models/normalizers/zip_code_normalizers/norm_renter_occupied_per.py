# Normalizer for % of renter occupied units
import norm_min_max
import numpy as np

def norm_renter_occupied_per(per):

	if per == None or np.isnan(per):
		per = 0.34448949

	return norm_min_max.norm_min_max(per,0.,1.)