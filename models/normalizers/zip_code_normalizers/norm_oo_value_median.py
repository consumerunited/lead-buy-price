# Normalizer for median value of ownere occupied house
import norm_min_max
import numpy as np

def norm_oo_value_median(value):

    oo_value_median = min(round(value/600000.,3),1)
    if np.isnan(oo_value_median):
        oo_value_median = -1

    return oo_value_median
	