# Normalizer for % of owner occupied units
import norm_min_max
import numpy as np

def norm_owner_occupied_per(per):

    owner_occupied_per = round(per,1)
    if np.isnan(owner_occupied_per):
        owner_occupied_per = -1

    return owner_occupied_per