# Normalizer for % of house holds with snap
import norm_min_max
import numpy as np

def norm_hsehlds_w_snap(time):

	if time == None or np.isnan(time):
		time = 0.14367173

	return norm_min_max.norm_min_max(time,0.,1.)