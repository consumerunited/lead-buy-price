# Normalizer for percent of househoulds that are married couples
import norm_min_max
import numpy as np


def norm_mrd_cpl_per(per):

    mrd_cpl_per = round(per,1)
    if np.isnan(mrd_cpl_per):
        mrd_cpl_per = -1

    return mrd_cpl_per
	