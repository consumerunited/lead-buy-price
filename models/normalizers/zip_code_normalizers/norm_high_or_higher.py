# Normalizer for % of people with high school or higher degree
import norm_min_max
import numpy as np

def norm_high_or_higher(per):

	if per == None  or np.isnan(per): 
		per = 85.18695

	return norm_min_max.norm_min_max(per/100.,0.,1.)