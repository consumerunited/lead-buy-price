# Helper function for normalizing between a min and max

def norm_min_max(value, min_val, max_val):

	if value < min_val:
		value = min_val
	elif value > max_val:
		value = max_val

	norm_value = (value-min_val)/(max_val-min_val)

	return norm_value