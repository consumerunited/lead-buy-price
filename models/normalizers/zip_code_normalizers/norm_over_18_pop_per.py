# Normalizer for over 18 population percentage
import norm_min_max
import numpy as np

def norm_over_18_pop_per(per):

	if per == None or np.isnan(per):
		per = .75

	return norm_min_max.norm_min_max(per,.2843,1.)
