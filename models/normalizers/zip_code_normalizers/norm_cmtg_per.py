# Normalizers for the percen of people commuting
import norm_min_max
import numpy as np

def norm_cmtg_per(per):

	if per == None or np.isnan(per):
		per = 0.87997989

	return norm_min_max.norm_min_max(per,0.,1.)
