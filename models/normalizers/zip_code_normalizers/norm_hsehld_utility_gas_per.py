# Normalizer for % of households gas
import numpy as np

def norm_hsehld_utility_gas_per(per):
        
    hsehld_utility_gas_per = round(per,1)
    if np.isnan(hsehld_utility_gas_per):
        hsehld_utility_gas_per = -1

    return hsehld_utility_gas_per
	