# Normalizer for vacancy rate
import numpy as np
import norm_min_max

def norm_vacancy_rate(per):

	if per == None or np.isnan(per):
		per = 2.39423

	return norm_min_max.norm_min_max(per/100.,0.,1.)