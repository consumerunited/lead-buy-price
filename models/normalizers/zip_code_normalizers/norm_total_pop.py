# Normalizer for total population
import numpy as np

def norm_total_pop(pop):

    total_pop = min(round(pop,-4)/100000.,1)
    if np.isnan(total_pop):
        total_pop = -1

    return total_pop