# Function for normailizing percent with children 6 to 17
import norm_min_max
import numpy as np

def norm_own_children_6_to_17(per):

	if per == None or np.isnan(per):
		per = 0.19570414

	if per > 1:
		per = 1


	return norm_min_max.norm_min_max(per,0.,1.)