# Normalizer for # of median rooms
import norm_min_max
import numpy as np

def norm_unit_median_rm(per):

    owner_occupied_per = round(per/9.,1)
    if np.isnan(owner_occupied_per):
        owner_occupied_per = -1

    return owner_occupied_per