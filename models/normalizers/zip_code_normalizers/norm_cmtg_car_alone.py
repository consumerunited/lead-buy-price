# Normalizer for % of people cmtg by car alone
import norm_min_max
import numpy as np

def norm_cmtg_car_alone(per):

	if per == None or np.isnan(per): 
		per = 0.79188615

	return norm_min_max.norm_min_max(per,0.,1.)