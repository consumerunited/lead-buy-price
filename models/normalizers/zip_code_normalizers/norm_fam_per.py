# Normalizer for percent of househoulds that are families
import norm_min_max
import numpy as np

def norm_fam_per(per):

	if per == None or np.isnan(per):
		per = 0.66946225

	return norm_min_max.norm_min_max(per,0.,1.)
	