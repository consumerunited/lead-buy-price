# Normalizer for % of people cmtg by car alone
import norm_min_max
import numpy as np

def norm_urban_cluster_pop_per(per):

	if per == None or np.isnan(per):
		per = 0.10990797

	return norm_min_max.norm_min_max(per,0.,1.)