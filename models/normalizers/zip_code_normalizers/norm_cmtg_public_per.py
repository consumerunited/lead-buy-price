# Normalizer for % of people with bachelors degree higher degree
import numpy as np

def norm_cmtg_public_per(per):

    cmtg_public_per = round(per,1)
    if np.isnan(cmtg_public_per):
        cmtg_public_per = -1

    return cmtg_public_per