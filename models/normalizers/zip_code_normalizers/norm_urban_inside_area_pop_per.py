# Normalizer for urban inside pop per
import norm_min_max
import numpy as np

def norm_urban_inside_area_pop_per(per):

	if per == None or np.isnan(per):
		per = 0.66487769

	return norm_min_max.norm_min_max(per,0.,1.)
	