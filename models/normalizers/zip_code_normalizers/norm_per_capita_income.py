# Normalizer for per capita income
import norm_min_max
import numpy as np

def norm_per_capita_income(per):

    per_capita_income = min(round(per,-4)/100000.,1)
    if np.isnan(per_capita_income):
        per_capita_income = -1

    return per_capita_income