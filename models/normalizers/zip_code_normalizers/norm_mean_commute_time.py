# Normalizer for mean commute time
import norm_min_max
import numpy as np

def norm_mean_commute_time(time):

	if time == None or np.isnan(time):
		time = 24.74874

	return norm_min_max.norm_min_max(time,0.,77.6)