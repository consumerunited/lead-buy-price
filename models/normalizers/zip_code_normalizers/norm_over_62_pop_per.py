# Normalize over 62 pop per, does not correlate linearlly with over 18 per
import norm_min_max
import numpy as np

def norm_over_62_pop_per(per):

	if per == None or np.isnan(per):
		per = 0.166

	return norm_min_max.norm_min_max(per,0.,1.)