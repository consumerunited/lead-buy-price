# Normalizer for urban population per
import norm_min_max
import numpy as np

def norm_urban_pop_per(per):

    urban_pop_per = round(per,1)
    if np.isnan(urban_pop_per):
        urban_pop_per = -1

    return urban_pop_per