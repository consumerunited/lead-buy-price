# Normalizer for percent of househoulds that are not families
import norm_min_max
import numpy as np

def norm_nonfam_hsehlds_per(per):

	if per == None or np.isnan(per):
		per = 0.33053781

	return norm_min_max.norm_min_max(per,0.,1.)
	