# Normalizer for people below pvty level
import norm_min_max
import numpy as np

def norm_people_blw_pvty_level(per):

	if per == None or np.isnan(per):
		per = 16.60796

	return norm_min_max.norm_min_max(per/100.,0.,.802)