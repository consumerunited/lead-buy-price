# Normalizer for % of people with bachelors degree higher degree
import numpy as np

def norm_bach_or_higher(per):

    overall_bach_or_hgher = min(round(per,0)/80.,1)
    if np.isnan(overall_bach_or_hgher):
        overall_bach_or_hgher = -1

    return overall_bach_or_hgher