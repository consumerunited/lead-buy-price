# Normalizer for % of households with 1 vehicls
import norm_min_max
import numpy as np

def norm_hsehld_1_vehicle_per(per):

	if per == None or np.isnan(per):
		per = 0.34298608

	return norm_min_max.norm_min_max(per,0.,1.)