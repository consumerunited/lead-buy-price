## Function for normalizing current liability limits

def norm_current_liability_limit_new(limit):

	if limit != None:
		limit = limit.upper()

	limit_none = 0
	limit_high = 0
	limit_low = 0

	if limit == None or limit == '':
		limit_none = 1
	elif limit in('100/300','150/300','250/500'):
		limit_high = 1
	else:
		limit_low = 1

	return limit_none, limit_high, limit_low