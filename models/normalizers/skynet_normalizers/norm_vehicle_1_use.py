# Function for normalizing vehicle use

def norm_vehicle_1_use(use):

	if use != None:
		use = use.upper()

	use_other = 0
	use_business = 0
	use_pleasure = 0
	use_work = 0

	if use == 'BUSINESS':
		use_business = 1
	elif use == 'PLEASURE':
		use_pleasure = 1
	elif use == 'WORK':
		use_work = 1
	else:
		use_other = 1

	return use_other, use_business, use_pleasure, use_work