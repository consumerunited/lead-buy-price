## Normalizer for driver 1 Credit

def norm_driver_1_credit_new(credit):

	if credit != None:
		credit = credit.upper()

	credit_high = 0
	credit_medium = 0
	credit_low =0
	credit_none = 0

	if credit in ('EXCELLENT','VERY GOOD'):
		credit_high = 1
	elif credit in ('FAIR','GOOD'):
		credit_medium = 1
	elif credit in ('BELOW FAIR', 'POOR','VERY POOR'):
		credit_low = 1
	else:
		credit_other = 1

	return credit_high, credit_medium, credit_low, credit_none