## Normalize email domain (john.smith@goji."COM")
## Certian domains regardless of age and other factors seem to move LTV

def norm_email_domain(domain):

	if domain != None:
		domain = domain.upper()

	high_domain = 0
	low_domain = 0

	if domain in ('NET','US','ORG'):
		high_domain = 1
	elif domain in ('EDU','CON'):
		low_domain = 1

	return high_domain, low_domain