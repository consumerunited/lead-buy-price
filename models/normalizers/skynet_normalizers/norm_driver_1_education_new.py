# Function normalizing education

def norm_driver_1_education_new(education):

	if education != None:
		education = education.upper().strip()

	education_associates = 0
	education_bachelors = 0
	education_high = 0
	education_masters = 0
	education_no_high = 0
	education_phd = 0
	education_some_college = 0
	education_other = 0

	if education == 'ASSOCIATES DEGREE': 
		education_associates = 1
	elif education == 'BACHELORS DEGREE':
		education_bachelors = 1
	elif education == 'HIGH SCHOOL DIPLOMA':
		education_high = 1
	elif education == 'MASTERS DEGREE':
		education_masters = 1
	elif education == 'NO HIGH SCHOOL DIPLOMA':
		education_no_high = 1
	elif education == 'PHD':
		education_phd = 1
	elif education == 'SOME COLLEGE':
		education_some_college = 1
	else:
		education_other = 1

	return education_associates, education_bachelors, education_high, education_masters, education_no_high, education_phd, education_some_college, education_other