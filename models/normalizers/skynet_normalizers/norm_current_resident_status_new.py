## Function for normalizin current resident status

def norm_current_resident_status_new(status):

	if status != None:
		status = status.upper()

	resident_own = 0
	resident_rent = 0
	resident_other = 0
	resident_unknown = 0

	if status == 'OWN':
		resident_own = 1
	elif status == 'RENT':
		resident_rent = 1
	elif status == 'OTHER':
		resident_other = 1
	else:
		resident_unknown = 1

	return resident_own, resident_rent, resident_other, resident_unknown
	