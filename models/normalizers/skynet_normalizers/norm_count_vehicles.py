# Function for Normalizing the number of vehicles

def norm_count_vehicles(count):

	if count == None or count == 0:
		count = 1

	if count > 3:
		count = 3

	count_norm = (count-1.)/(3.-1.)

	return count_norm