# Function for standardizing carriers to be addressed by model

def norm_current_insurance_carrier_new(carrier):

	if carrier != None:
		carrier = carrier.upper()

	no_prior = 0
	pref_prior = 0
	std_prior = 0
	nstd_prior = 0
	unsure_prior = 0

	if carrier == None or carrier == '':
		no_prior = 1
	elif carrier in ('HARTFORD AARP','AMICA','NATIONWIDE','LIBERTY MUTUAL','ALLSTATE','PRUDENTIAL','KEMPER DIRECT','PEMCO','HARTFORD AGENCY','NATIONAL CASUALTY','EMC','FARMERS','TRAVELERS','METROPOLITAN','SAFECO'):
		pref_prior = 1
	elif carrier in ('DIRECT GENERAL INSURANCE','SENTRY','AAA MID-ATLANTIC','USAA','FARMLAND','SUPERIOR','COTTON STATES','AIG','TRI-STATE CONSUMER','GRANGE','GEICO','GENERAL CASUALTY','PACIFIC INSURANCE','INDEPENDENT','PATRIOT GENERAL','NATIONAL INSURANCE','SHELTER','PROGRESSIVE','LUMBERMENS MUTUAL','SENTINEL INSURANCE','UNITRIN','ESURANCE','21ST CENTURY','FARM BUREAU MUTUAL (IA GROUP)','ERIE','AMERICAN FAMILY','HALLMARK','WOODLANDS FINANCIAL GROUP','HANOVER','PLYMOUTH ROCK ASSURANCE','STANDARD FIRE INSURANCE CO','COUNTRY','ALLIED','STATE FARM'):
		std_prior = 1
	elif carrier in ('UNSURE','OTHER'):
		unsure_prior = 1
	else:
		nstd_prior = 1

	return no_prior, pref_prior, std_prior, nstd_prior, unsure_prior
