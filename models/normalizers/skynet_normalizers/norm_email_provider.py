# Some email providers have on average better LTV than others
# Some email providers have materially worse LTV than others
# This seems to be independent of age and other strongly correlated factors

def norm_email_provider(provider):

	if provider != None:
		provider = provider.upper()

	high_provider = 0
	low_provider = 0

	if provider in ('OPTONLINE','VERIZON','FRONTIER','SBCGLOBAL','COMCAST','COX','MSN','JUNO','WINDSTREAM'):
		high_provider = 1
	elif provider in ('YAHOO','GMAIL','ROCKETMAIL','YMAIL'):
		low_provider = 1

	return high_provider, low_provider