## Function for normalizing current insurance length

def norm_current_insurance_length_new(length):

	if length != None:
		length = length.upper()

	prior_none = 0
	prior_less_than_one = 0
	prior_one_to_two = 0
	prior_more_than_two = 0

	if length == None or length == 0:
		prior_none = 1
	elif length == 'LESS THAN 1 YEAR':
		prior_less_than_one = 1
	elif length == '1-2 YEARS':
		prior_one_to_two = 1
	else:
		prior_more_than_two = 1

	return prior_none, prior_less_than_one, prior_one_to_two, prior_more_than_two
