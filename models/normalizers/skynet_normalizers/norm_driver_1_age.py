## Function for normalizing driver 1 age

def norm_driver_1_age(age):

	if age == None:
		age = 30

	if type(age) == str:
		age = int(age)

	# Bind Age between reasonable values
	if age > 100:
		age = 100
	elif age < 16:
		age = 16

	# Noramlize between min and max
	norm_age = (age-16)/(100.-16.)

	return norm_age