# Normalize the drivers gender

def norm_driver_1_gender(gender):

	if gender != None:
		gender = gender.upper()

	gender_male = 0
	gender_female = 0
	gender_other = 0

	if gender == 'MALE':
		gender_male = 1
	elif gender == 'FEMALE':
		gender_female = 1
	else:
		gender_other = 0

	return gender_male, gender_female, gender_other