## Standardize Jornaya entities

def jornaya_norm_entities(entities):
  
    try:
        if entities != 'nan':
            jornaya_entities = int(entities)/15.0
        else:
            jornaya_entities = -1
    except:
        jornaya_entities = -1    

    return jornaya_entities
        