## Standardize Jornaya lead duration

def jornaya_norm_duration(duration):

    try:
        if duration != 'nan':
            jornaya_duration = int(duration)/7.
        else:
            jornaya_duration = -1
    except:
        jornaya_duration = -1

    return jornaya_duration