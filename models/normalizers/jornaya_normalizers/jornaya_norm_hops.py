## Standardize Jornaya lead hops

def jornaya_norm_hops(hops):

    try:
        if hops != 'nan':
            jornaya_hops = int(hops)/2.0
        else:
            jornaya_hops = -1
    except:
        jornaya_hops = -1

    return jornaya_hops