## Standardize Jornaya lead age

def jornaya_norm_age(age):

    try:
        if age != 'nan':
            jornaya_age = int(age)/10.
        else:
            jornaya_age = -1
    except:
        jornaya_age = -1

    return jornaya_age