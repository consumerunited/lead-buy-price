# Normalize day of month

import calendar

def norm_day_of_month(date):

	days_in_month = calendar.monthrange(date.year,date.month)[1]

	day = date.day/float(days_in_month)

	return day
