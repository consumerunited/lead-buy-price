## Standardize weekday

def norm_weekday(weekday):

    weekday = weekday.weekday()

    weekday_mon = 0
    weekday_tue = 0
    weekday_wed = 0
    weekday_thur = 0
    weekday_fri = 0
    weekday_sat = 0
    weekday_sun = 0

    if weekday == 0:
        weekday_mon = 1
    elif weekday == 1:
        weekday_tue = 1
    elif weekday == 2:
        weekday_wed = 1
    elif weekday == 3:
        weekday_thur = 1
    elif weekday == 4:
        weekday_fri = 1
    elif weekday == 5:
        weekday_sat = 1
    else:
        weekday_sun = 1
        
    return weekday_mon, weekday_tue, weekday_wed, weekday_thur, weekday_fri, weekday_sat, weekday_sun