import random
import os
import pandas as pd
import numpy as np
import xgboost
import cPickle as pickle

from normalize_data_for_conversion import normalize_data_for_conversion

import xgboost

# Load Model
model_version = 1
clf = pickle.load(open(os.environ['APP_ROOT'] + "/models/lead_score/conversion/model_config/version_1/lead_score_conversion_v1.pkl", "rb"))

def get_conversion(lead_data, zip_data, conversion_data):

	# Normalize Data
	fields, form_data = normalize_data_for_conversion(lead_data, zip_data, conversion_data)

	# Add the Bias
	form_data.append(1)
	fields.append('bias')

	# Convert to how sklearn wants it
	pred_data = pd.DataFrame(np.array(form_data).reshape(1, -1),columns=fields,dtype=np.float32).drop(['lead_id','converted'],axis=1)

	# Raw Conversion Prediction
	pred = clf.predict_proba(pred_data)[0][1]

	# Convert Prediction to Band
	if pred <=0.0423154: 
		band = 0
	elif pred <=0.054878: 
		band = 1
	elif pred <=0.0702245:
		band = 2
	elif pred <=0.089113: 
		band = 3
	elif pred <=0.115344: 
		band = 4
	elif pred <=0.149902: 
		band = 5
	elif pred <=0.185054: 
		band = 6
	elif pred <=0.209769: 
		band = 7
	elif pred <=0.230296: 
		band = 8
	elif pred <=0.24646: 
		band = 9
	elif pred <=0.259911: 
		band = 10
	elif pred <=0.271158: 
		band = 11
	elif pred <=0.281463: 
		band = 12
	elif pred <=0.290071: 
		band = 13
	elif pred <=0.297327: 
		band = 14
	elif pred <=0.303961: 
		band = 15
	elif pred <=0.310292: 
		band = 16
	elif pred <=0.315635: 
		band = 17
	elif pred <=0.320796: 
		band = 18
	elif pred <=0.325465: 
		band = 19
	elif pred <=0.329985: 
		band = 20
	elif pred <=0.334338: 
		band = 21
	elif pred <=0.338367: 
		band = 22
	elif pred <=0.342287: 
		band = 23
	elif pred <=0.345956: 
		band = 24
	elif pred <=0.349753: 
		band = 25
	elif pred <=0.353203: 
		band = 26
	elif pred <=0.35657: 
		band = 27
	elif pred <=0.359731: 
		band = 28
	elif pred <=0.36279: 
		band = 29
	elif pred <=0.365872: 
		band = 30
	elif pred <=0.368883: 
		band = 31
	elif pred <=0.371759: 
		band = 32
	elif pred <=0.374536: 
		band = 33
	elif pred <=0.377321: 
		band = 34
	elif pred <=0.379886: 
		band = 35
	elif pred <=0.382483: 
		band = 36
	elif pred <=0.385309: 
		band = 37
	elif pred <=0.387804: 
		band = 38
	elif pred <=0.390277: 
		band = 39
	elif pred <=0.392749: 
		band = 40
	elif pred <=0.395295: 
		band = 41
	elif pred <=0.397822: 
		band = 42
	elif pred <=0.400284: 
		band = 43
	elif pred <=0.402782: 
		band = 44
	elif pred <=0.405176: 
		band = 45
	elif pred <=0.407452: 
		band = 46
	elif pred <=0.409879: 
		band = 47
	elif pred <=0.412284: 
		band = 48
	elif pred <=0.414736: 
		band = 49
	elif pred <=0.417091: 
		band = 50
	elif pred <=0.419293: 
		band = 51
	elif pred <=0.421737: 
		band = 52
	elif pred <=0.424042: 
		band = 53
	elif pred <=0.426307: 
		band = 54
	elif pred <=0.428706: 
		band = 55
	elif pred <=0.431046: 
		band = 56
	elif pred <=0.433376: 
		band = 57
	elif pred <=0.435685: 
		band = 58
	elif pred <=0.437933: 
		band = 59
	elif pred <=0.440269: 
		band = 60
	elif pred <=0.442706: 
		band = 61
	elif pred <=0.445101: 
		band = 62
	elif pred <=0.447437: 
		band = 63
	elif pred <=0.449914: 
		band = 64
	elif pred <=0.452331: 
		band = 65
	elif pred <=0.45501: 
		band = 66
	elif pred <=0.457706: 
		band = 67
	elif pred <=0.46048: 
		band = 68
	elif pred <=0.463231: 
		band = 69
	elif pred <=0.465994: 
		band = 70
	elif pred <=0.468851: 
		band = 71
	elif pred <=0.47176: 
		band = 72
	elif pred <=0.474739: 
		band = 73
	elif pred <=0.477795: 
		band = 74
	elif pred <=0.481065: 
		band = 75
	elif pred <=0.484569: 
		band = 76
	elif pred <=0.487869: 
		band = 77
	elif pred <=0.491783: 
		band = 78
	elif pred <=0.49576: 
		band = 79
	elif pred <=0.499891: 
		band = 80
	elif pred <=0.504171: 
		band = 81
	elif pred <=0.508597: 
		band = 82
	elif pred <=0.513323: 
		band = 83
	elif pred <=0.518829: 
		band = 84
	elif pred <=0.524429: 
		band = 85
	elif pred <=0.530653: 
		band = 86
	elif pred <=0.538: 
		band = 87
	elif pred <=0.545712: 
		band = 88
	elif pred <=0.55535: 
		band = 89
	elif pred <=0.564927: 
		band = 90
	elif pred <=0.575722: 
		band = 91
	elif pred <=0.5886: 
		band = 92
	elif pred <=0.604859: 
		band = 93
	elif pred <=0.627403: 
		band = 94
	elif pred <=0.66727: 
		band = 95
	elif pred <=0.77151: 
		band = 96
	elif pred <=0.878187: 
		band = 97
	elif pred <=0.914017: 
		band = 98
	elif pred <=0.930466: 
		band = 99
	elif pred <=0.956887: 
		band = 100

	return pred, band, model_version
