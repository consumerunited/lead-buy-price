# This is a placeholder for given a lead forecast its ltv
import random
from normalize_data_for_ltv_update import normalize_ltv_data
from sklearn.externals import joblib
import numpy as np
import os
import pandas as pd

# Import the model
model_version = 2
model = joblib.load(os.environ['APP_ROOT'] +  "/models/lead_price/ltv/model_config/version_2/xgb_ltv.pkl")

def get_ltv(lead_data, zip_data):

	# Transform Data
	fields, form_data = normalize_ltv_data(lead_data, zip_data, train_flag = 0)

	df = pd.DataFrame([form_data],columns=fields)
	df['bias'] = 1
	df.drop(['lead_id','total_ltv'],axis = 1, inplace=True)

	# Add the Bias
	form_data.append(1)

	# Predict
	ltv = float(model.predict(df)[0])

	return ltv, model_version
