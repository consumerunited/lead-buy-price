# This is a placeholder for given a lead forecast its conversion
import random
import os
import pandas as pd
from cache.cache import cache
import numpy as np
from scipy.special import erfc
import math

from normalize_data_for_conversion_update import normalize_data_for_conversion

model_version = 3

# Load Model
from sklearn.ensemble import BaggingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.externals import joblib

# model = joblib.load(os.environ['APP_ROOT'] +  "/models/lead_price/conversion/model_config/version_1/conversion_model.pkl")

xgb_model = joblib.load(os.environ['APP_ROOT'] +  "/models/lead_price/conversion/model_config/version_3/xgb/xgb_conversion.pkl")
lin_model = joblib.load(os.environ['APP_ROOT'] +  "/models/lead_price/conversion/model_config/version_3/lin/lin_conversion.pkl")
gbr_model = joblib.load(os.environ['APP_ROOT'] +  "/models/lead_price/conversion/model_config/version_3/gbr/gbr_conversion.pkl")
nn_model = joblib.load(os.environ['APP_ROOT'] +  "/models/lead_price/conversion/model_config/version_3/nn/nn_conversion.pkl")
pipe_model = joblib.load(os.environ['APP_ROOT'] +  "/models/lead_price/conversion/model_config/version_3/pipe/pipe_conversion.pkl")
stack_model = joblib.load(os.environ['APP_ROOT'] +  "/models/lead_price/conversion/model_config/version_3/stack/stack_conversion.pkl")
def convert_to_conversion(prediction):
    
    if prediction <= 0.037697885: 
        conversion = 0.00219671854829118
    elif prediction <= 0.06920996: 
        conversion = 0.00233101059292497
    elif prediction <= 0.089478895: 
        conversion = 0.00266738065926761
    elif prediction <= 0.10564187: 
        conversion = 0.00327774981667758
    elif prediction <= 0.13640043: 
        conversion = 0.0033891216355117
    elif prediction <= 0.18146019: 
        conversion = 0.00379141965389411
    elif prediction <= 0.20774433: 
        conversion = 0.00411042550597565
    elif prediction <= 0.22609623: 
        conversion = 0.00443796191836803
    elif prediction <= 0.23910098: 
        conversion = 0.00473491923382016
    elif prediction <= 0.2508621: 
        conversion = 0.00496597666899241
    elif prediction <= 0.26187232: 
        conversion = 0.00521287210972916
    elif prediction <= 0.27153254: 
        conversion = 0.00542033299310686
    elif prediction <= 0.28007776: 
        conversion = 0.00558107270393134
    elif prediction <= 0.2880145: 
        conversion = 0.0057107528432006
    elif prediction <= 0.29570436: 
        conversion = 0.00588705651411909
    elif prediction <= 0.30310732: 
        conversion = 0.0061308365590796
    elif prediction <= 0.31065843: 
        conversion = 0.00634347241036811
    elif prediction <= 0.31797087: 
        conversion = 0.00654568021311182
    elif prediction <= 0.32543382: 
        conversion = 0.00674793032691392
    elif prediction <= 0.3324095: 
        conversion = 0.00698644752715005
    elif prediction <= 0.33946487: 
        conversion = 0.00723022431741376
    elif prediction <= 0.3463471: 
        conversion = 0.00747397832479988
    elif prediction <= 0.353416: 
        conversion = 0.00779030556141564
    elif prediction <= 0.35970342: 
        conversion = 0.0081585712495538
    elif prediction <= 0.36579636: 
        conversion = 0.00851123717593318
    elif prediction <= 0.37165076: 
        conversion = 0.00876541923186409
    elif prediction <= 0.37727612: 
        conversion = 0.00890544930695495
    elif prediction <= 0.38249543: 
        conversion = 0.00900914069228221
    elif prediction <= 0.38762665: 
        conversion = 0.00915962810818906
    elif prediction <= 0.392418: 
        conversion = 0.00932046220522073
    elif prediction <= 0.39683425: 
        conversion = 0.00945539867982077
    elif prediction <= 0.40129355: 
        conversion = 0.00964217921973604
    elif prediction <= 0.40569037: 
        conversion = 0.00982888815632173
    elif prediction <= 0.4098214: 
        conversion = 0.00993270647482416
    elif prediction <= 0.41407987: 
        conversion = 0.00998461563407538
    elif prediction <= 0.41844708: 
        conversion = 0.0100416541954822
    elif prediction <= 0.42284232: 
        conversion = 0.0101609778895363
    elif prediction <= 0.42715585: 
        conversion = 0.0103788114916132
    elif prediction <= 0.43194187: 
        conversion = 0.0106589464998213
    elif prediction <= 0.4362119: 
        conversion = 0.0108041417787325
    elif prediction <= 0.4401951: 
        conversion = 0.0109181472982166
    elif prediction <= 0.44406942: 
        conversion = 0.0110270266702419
    elif prediction <= 0.447603: 
        conversion = 0.0111047781220791
    elif prediction <= 0.45114392: 
        conversion = 0.0111307375837499
    elif prediction <= 0.45474175: 
        conversion = 0.0112137063145564
    elif prediction <= 0.45835334: 
        conversion = 0.0113277215981309
    elif prediction <= 0.46178785: 
        conversion = 0.0114729819709781
    elif prediction <= 0.465358: 
        conversion = 0.0116026295632794
    elif prediction <= 0.46885756: 
        conversion = 0.0116908285918422
    elif prediction <= 0.47208336: 
        conversion = 0.0117738168508295
    elif prediction <= 0.47504893: 
        conversion = 0.0119656747177516
    elif prediction <= 0.47832543: 
        conversion = 0.012173220223246
    elif prediction <= 0.4813817: 
        conversion = 0.0124014623456869
    elif prediction <= 0.48506436: 
        conversion = 0.012608884172703
    elif prediction <= 0.4884202: 
        conversion = 0.0128786008964568
    elif prediction <= 0.49178424: 
        conversion = 0.0131845293767989
    elif prediction <= 0.4952417: 
        conversion = 0.013392048844719
    elif prediction <= 0.49876216: 
        conversion = 0.0135787577813047
    elif prediction <= 0.5026919: 
        conversion = 0.0137653788410768
    elif prediction <= 0.5065671: 
        conversion = 0.0139728267056673
    elif prediction <= 0.510007: 
        conversion = 0.0141750865835598
    elif prediction <= 0.5140174: 
        conversion = 0.0143411054125927
    elif prediction <= 0.51836926: 
        conversion = 0.0145278143491784
    elif prediction <= 0.5228766: 
        conversion = 0.0146885084942477
    elif prediction <= 0.5270607: 
        conversion = 0.0148597153099785
    elif prediction <= 0.5314629: 
        conversion = 0.0150204908224678
    elif prediction <= 0.53608775: 
        conversion = 0.0150309709461614
    elif prediction <= 0.54044765: 
        conversion = 0.015165881383187
    elif prediction <= 0.5448499: 
        conversion = 0.0152177254485023
    elif prediction <= 0.54992336: 
        conversion = 0.0152749332541427
    elif prediction <= 0.55445683: 
        conversion = 0.0153165087510563
    elif prediction <= 0.55879796: 
        conversion = 0.0154150316778663
    elif prediction <= 0.5634951: 
        conversion = 0.0155343911735851
    elif prediction <= 0.56806016: 
        conversion = 0.0156588182322204
    elif prediction <= 0.57247543: 
        conversion = 0.0157730190335124
    elif prediction <= 0.57699007: 
        conversion = 0.0159701592733397
    elif prediction <= 0.58162194: 
        conversion = 0.0161412651934698
    elif prediction <= 0.5863375: 
        conversion = 0.01644207078105
    elif prediction <= 0.5912463: 
        conversion = 0.0167531351729413
    elif prediction <= 0.59621376: 
        conversion = 0.0172147683481596
    elif prediction <= 0.60135996: 
        conversion = 0.0176445331596679
    elif prediction <= 0.6068856: 
        conversion = 0.0179660569265608
    elif prediction <= 0.61248493: 
        conversion = 0.018331150505831
    elif prediction <= 0.61808515: 
        conversion = 0.0186987567110302
    elif prediction <= 0.6239551: 
        conversion = 0.0191319145439518
    elif prediction <= 0.62968075: 
        conversion = 0.0196584830032159
    elif prediction <= 0.63522255: 
        conversion = 0.0200686164861941
    elif prediction <= 0.6415984: 
        conversion = 0.0203858344199906
    elif prediction <= 0.6484624: 
        conversion = 0.0209631632758347
    elif prediction <= 0.65453434: 
        conversion = 0.0221288039473422
    elif prediction <= 0.6610669: 
        conversion = 0.0232971749752766
    elif prediction <= 0.6675038: 
        conversion = 0.0248355402472313
    elif prediction <= 0.6750462:
        conversion = 0.0265171011288204
    elif prediction <= 0.68382293: 
        conversion = 0.0281353137633058
    elif prediction <= 0.69225085: 
        conversion = 0.0300116352097209
    elif prediction <= 0.7033693: 
        conversion = 0.031770592128704
    elif prediction <= 0.7160595: 
        conversion = 0.0333907461565987
    elif prediction <= 0.7316368: 
        conversion = 0.0349933940638158
    elif prediction <= 0.75200456: 
        conversion = 0.0359815216854403
    elif prediction <= 0.7927351: 
        conversion = 0.0369826969225442
    else: 
        conversion = 0.0378623491181171
   
    return conversion

def get_conversion(lead_data, zip_data, conversion_data, jornaya_data):
    fields, form_data = normalize_data_for_conversion(lead_data, zip_data, conversion_data, jornaya_data, train_flag = 0)

    df = pd.DataFrame([form_data],columns=fields)
    df['bias'] = 1
    df.drop(['lead_id','converted'],axis = 1, inplace=True)

    # Make Pre Predictions
    xgb_pred = xgb_model.predict_proba(df)[0][1].round(4)
    lin_pred = lin_model.predict_proba(df)[0][1].round(4)
    gbr_pred = gbr_model.predict_proba(df)[0][1].round(4)
    nn_pred = nn_model.predict_proba(df)[0][1].round(4)
    pipe_pred = pipe_model.predict_proba(df)[0][1].round(4)

    # Stack with input
    pred_df = pd.DataFrame([[xgb_pred, lin_pred, gbr_pred, nn_pred, pipe_pred]],columns=['xgb_pred', 'lin_pred', 'gbr_pred', 'nn_pred', 'pipe_pred'])
    pred_df = pred_df.join(df[['source_conversion','norm_age','campaign_conversion','state_conversion','zip_short_conversion','zip_median_earning','zip_oo_value_median']])
    stack_pred = pipe_model.predict_proba(df)[0][1]

    st_conversion = convert_to_conversion(stack_pred)

    # Conversion    
    conversion = st_conversion

    return conversion, st_conversion, 0, model_version
