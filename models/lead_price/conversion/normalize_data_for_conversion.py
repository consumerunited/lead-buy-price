# Function for getting data data ready to be fed to Conversion Model

# Sky net normalizers
from models.normalizers.skynet_normalizers import norm_current_insurance_carrier_new
from models.normalizers.skynet_normalizers import norm_current_resident_status_new
from models.normalizers.skynet_normalizers import norm_current_insurance_length_new
from models.normalizers.skynet_normalizers import norm_current_liability_limit_new
from models.normalizers.skynet_normalizers import norm_driver_1_gender
from models.normalizers.skynet_normalizers import norm_driver_1_marital_status_new
from models.normalizers.skynet_normalizers import norm_driver_1_age
from models.normalizers.skynet_normalizers import norm_driver_1_education_new
from models.normalizers.skynet_normalizers import norm_driver_1_credit_new
from models.normalizers.skynet_normalizers import norm_count_drivers
from models.normalizers.skynet_normalizers import norm_vehicle_age
from models.normalizers.skynet_normalizers import norm_vehicle_1_use
from models.normalizers.skynet_normalizers import norm_count_vehicles
from models.normalizers.skynet_normalizers import norm_email_provider
from models.normalizers.skynet_normalizers import norm_email_domain

# Goji normalizers
from models.normalizers.goji_normalizers import goji_norm_prior_policy_carrier
from models.normalizers.goji_normalizers import goji_norm_home_ownership
from models.normalizers.goji_normalizers import goji_norm_prior_policy_length
from models.normalizers.goji_normalizers import goji_norm_prior_policy_limit
from models.normalizers.goji_normalizers import goji_norm_primary_driver_gender
from models.normalizers.goji_normalizers import goji_norm_primary_driver_marital_status
from models.normalizers.goji_normalizers import goji_norm_primary_driver_age
from models.normalizers.goji_normalizers import goji_norm_primary_driver_education
from models.normalizers.goji_normalizers import goji_norm_primary_driver_credit
from models.normalizers.goji_normalizers import goji_norm_count_drivers
from models.normalizers.goji_normalizers import goji_norm_vehicle_age
from models.normalizers.goji_normalizers import goji_norm_vehicle_use
from models.normalizers.goji_normalizers import goji_norm_count_vehicles
from models.normalizers.goji_normalizers import goji_norm_email_provider
from models.normalizers.goji_normalizers import goji_norm_email_domain

# Zip Code Normalizers
from models.normalizers.zip_code_normalizers import norm_median_age
from models.normalizers.zip_code_normalizers import norm_over_18_pop_per
from models.normalizers.zip_code_normalizers import norm_over_62_pop_per
from models.normalizers.zip_code_normalizers import norm_high_or_higher
from models.normalizers.zip_code_normalizers import norm_bach_or_higher
from models.normalizers.zip_code_normalizers import norm_cmtg_per
from models.normalizers.zip_code_normalizers import norm_cmtg_car_alone
from models.normalizers.zip_code_normalizers import norm_mean_commute_time
from models.normalizers.zip_code_normalizers import norm_hsehlds_w_snap
from models.normalizers.zip_code_normalizers import norm_per_capita_income
from models.normalizers.zip_code_normalizers import norm_median_earning
from models.normalizers.zip_code_normalizers import norm_people_blw_pvty_level
from models.normalizers.zip_code_normalizers import norm_vacancy_rate
from models.normalizers.zip_code_normalizers import norm_owner_occupied_per
from models.normalizers.zip_code_normalizers import norm_renter_occupied_per
from models.normalizers.zip_code_normalizers import norm_median_rm
from models.normalizers.zip_code_normalizers import norm_hsehld_1_vehicle_per
from models.normalizers.zip_code_normalizers import norm_hsehld_2_vehicle_per
from models.normalizers.zip_code_normalizers import norm_hsehld_3_or_more_vehicle_per
from models.normalizers.zip_code_normalizers import norm_hsehld_electricity_per
from models.normalizers.zip_code_normalizers import norm_oo_value_median
from models.normalizers.zip_code_normalizers import norm_oo_w_mrtg_per
from models.normalizers.zip_code_normalizers import norm_fam_per
from models.normalizers.zip_code_normalizers import norm_mrd_cpl_per
from models.normalizers.zip_code_normalizers import norm_nonfam_hsehlds_per
from models.normalizers.zip_code_normalizers import norm_urban_pop_per
from models.normalizers.zip_code_normalizers import norm_urban_inside_area_pop_per
from models.normalizers.zip_code_normalizers import norm_urban_cluster_pop_per
from models.normalizers.zip_code_normalizers import norm_rural_pop_per

def normalize_data_for_conversion(data, zip_data, conversion_data, old_flag = 1):

    if old_flag == 1:
        # Normalize Prior Carrier
        no_prior, pref_prior, std_prior, nstd_prior, unsure_prior = norm_current_insurance_carrier_new.norm_current_insurance_carrier_new(data['current_insurance_carrier_new'])

        # Normalize Resident Status
        resident_own, resident_rent, resident_other, resident_unknown = norm_current_resident_status_new.norm_current_resident_status_new(data['current_resident_status_new'])

        # Normalize Current Coverage Length
        prior_none, prior_less_than_one, prior_one_to_two, prior_more_than_two = norm_current_insurance_length_new.norm_current_insurance_length_new(data['current_insurance_coverage_length_new'])

        # Normalize Current Liability Limit
        limit_none, limit_high, limit_low = norm_current_liability_limit_new.norm_current_liability_limit_new(data['current_liability_limits_new'])

        # Driver Gender
        gender_male, gender_female, gender_other = norm_driver_1_gender.norm_driver_1_gender(data['driver_1_gender'])

        # Driver Marital Status
        marital_divorced, marital_married, marital_other, marital_single, marital_widowed = norm_driver_1_marital_status_new.norm_driver_1_marital_status_new(data['driver_1_marital_status_new'])

        # Normalize Driver Ages
        norm_age = norm_driver_1_age.norm_driver_1_age(data['driver_1_age'])

        # Normalize Driver Education
        education_associates, education_bachelors, education_high, education_masters, education_no_high, education_phd, education_some_college, education_other = norm_driver_1_education_new.norm_driver_1_education_new(data['driver_1_highest_degree_new'])

        # Normalize Driver Credit
        credit_high, credit_medium, credit_low, credit_none = norm_driver_1_credit_new.norm_driver_1_credit_new(data['driver_1_credit_new'])

        # Normalize Number of Drivers
        norm_drivers = norm_count_drivers.norm_count_drivers(data['count_drivers'])

        # Normalize Age of Vehicle
        vehicle_age = norm_vehicle_age.norm_vehicle_age(data['vehicle_1_age_old'])

        # Normalize vehicle use
        use_other, use_business, use_pleasure, use_work = norm_vehicle_1_use.norm_vehicle_1_use(data['vehicle_1_vehicle_use_new'])

        # Normalize number of vehicles
        norm_vehicles = norm_count_vehicles.norm_count_vehicles(data['count_vehicles'])

        # Normalize Email Provider
        high_provider, low_provider = norm_email_provider.norm_email_provider(data['email_provider'])

        # Normalize Email Domain
        high_domain, low_domain = norm_email_domain.norm_email_domain(data['email_domain'])

        # Conversion
        converted = data['converted']

    else:
        # Normalize Prior Carrier
        missing_prior, no_prior, pref_prior, std_prior, nstd_prior, unsure_prior = goji_norm_prior_policy_carrier.goji_norm_prior_policy_carrier(data['prior_carrier'])

        # Normalize Resident Status
        resident_own, resident_rent, resident_other, resident_unknown = goji_norm_home_ownership.goji_norm_home_ownership(data['homeownership'])

        # Normalize Current Coverage Length
        prior_none, prior_less_than_one, prior_one_to_two, prior_more_than_two = goji_norm_prior_policy_length.goji_norm_prior_policy_length(data['months_with_prior_carrier'])

        # Normalize Current Liability Limit
        limit_none, limit_high, limit_low = goji_norm_prior_policy_limit.goji_norm_prior_policy_limit(data['liability_limit'])

        # Driver Gender
        gender_male, gender_female, gender_other = goji_norm_primary_driver_gender.goji_norm_primary_driver_gender(data['gender'])

        # Driver Marital Status
        marital_divorced, marital_married, marital_other, marital_single, marital_widowed = goji_norm_primary_driver_marital_status.goji_norm_primary_driver_marital_status(data['marital_status'])

        # Normalize Driver Ages
        norm_age = goji_norm_primary_driver_age.goji_norm_primary_driver_age(data['age'])

        # Normalize Driver Education
        education_associates, education_bachelors, education_high, education_masters, education_no_high, education_phd, education_some_college, education_other = goji_norm_primary_driver_education.goji_norm_primary_driver_education(data['education'])

        # Normalize Driver Credit
        credit_high, credit_medium, credit_low, credit_none = goji_norm_primary_driver_credit.goji_norm_primary_driver_credit(data['credit'])

        # Normalize Number of Drivers
        norm_drivers = goji_norm_count_drivers.goji_norm_count_drivers(data['drivers'])

        # Normalize Age of Vehicle
        vehicle_age = goji_norm_vehicle_age.goji_norm_vehicle_age(data['vehicle_age'])

        # Normalize vehicle use
        use_other, use_business, use_pleasure, use_work = goji_norm_vehicle_use.goji_norm_vehicle_use(data['vehicle_use'])

        # Normalize number of vehicles
        norm_vehicles = goji_norm_count_vehicles.goji_norm_count_vehicles(data['vehicles'])

        # Normalize Email Provider
        high_provider, low_provider = goji_norm_email_provider.goji_norm_email_provider(data['email'])

        # Normalize Email Domain
        high_domain, low_domain = goji_norm_email_domain.goji_norm_email_domain(data['email'])

        # Total LTV
        converted = None

    # Median age in a zip code
    zip_median_age = norm_median_age.norm_median_age(zip_data['median_age'])

    # Over 18 Percentage in a zip code
    zip_over_18_per_norm = norm_over_18_pop_per.norm_over_18_pop_per(zip_data['over_18_pop_per'])

    # Over 62 percentage in a zip code
    zip_over_62_per_norm = norm_over_62_pop_per.norm_over_62_pop_per(zip_data['over_62_pop_per'])

    # Percent with high school or higher degree
    zip_high_or_higher = norm_high_or_higher.norm_high_or_higher(zip_data['overall_hgh_or_hgher'])

    # Percent with bachelors or higher degree
    zip_bach_or_higher = norm_bach_or_higher.norm_bach_or_higher(zip_data['overall_bach_or_hgher'])

    # Percent commutings
    zip_cmtg_per = norm_cmtg_per.norm_cmtg_per(zip_data['cmtg_per'])

    # Percent commuting by car alone
    zip_cmtg_car_alone = norm_cmtg_car_alone.norm_cmtg_car_alone(zip_data['cmtg_car_alone_per'])

    # Mean Commute Time
    zip_mean_commute_time = norm_mean_commute_time.norm_mean_commute_time(zip_data['mean_commute_time'])

    # Household w/ SNAP Per
    zip_hsehlds_w_snap = norm_hsehlds_w_snap.norm_hsehlds_w_snap(zip_data['households_w_snap_per'])

    # Per Capita Income
    zip_per_capita_income = norm_per_capita_income.norm_per_capita_income(zip_data['per_capita_income'])

    # Median Earnings
    zip_median_earning = norm_median_earning.norm_median_earning(zip_data['median_earning'])

    # Percentage Pelow Poverty Level
    zip_below_pvty_level = norm_people_blw_pvty_level.norm_people_blw_pvty_level(zip_data['all_people_blw_pvty_level'])

    # Vacancy Rate
    zip_vacancy_rate = norm_vacancy_rate.norm_vacancy_rate(zip_data['homeowner_vacancy_rate'])

    # Owner occupied per
    zip_owner_occupied_per = norm_owner_occupied_per.norm_owner_occupied_per(zip_data['owner_occupied_per'])

    # Renter occupied per
    zip_renter_occupied_per = norm_renter_occupied_per.norm_renter_occupied_per(zip_data['renter_occupied_per'])

    # Median Number of rooms
    zip_median_rm = norm_median_rm.norm_median_rm(zip_data['unit_median_rm'])

    # Percent of households with 1 vehciles
    zip_hsehld_1_vehicle = norm_hsehld_1_vehicle_per.norm_hsehld_1_vehicle_per(zip_data['hsehld_1_vehicle_per'])

    # Percent of households with 2 vehciles
    zip_hsehld_2_vehicle = norm_hsehld_2_vehicle_per.norm_hsehld_2_vehicle_per(zip_data['hsehld_2_vehicles_per'])

    # PErcent of households with 3 or more vehicles
    zip_hsehld_3_or_more_vehicle = norm_hsehld_3_or_more_vehicle_per.norm_hsehld_3_or_more_vehicle_per(zip_data['hsehld_3_or_more_vehicle_per'])

    # Percent of households with electric heat
    zip_hsehld_electricity_per = norm_hsehld_electricity_per.norm_hsehld_electricity_per(zip_data['hsehld_electricity_per'])

    # Owner occupied median home value
    zip_oo_value_median = norm_oo_value_median.norm_oo_value_median(zip_data['oo_value_median'])

    # Owner occupied with mrtg
    zip_oo_mrtg_per = norm_oo_w_mrtg_per.norm_oo_w_mrtg_per(zip_data['oo_w_mrtg_per'])

    # Fam Per
    zip_fam_per = norm_fam_per.norm_fam_per(zip_data['fam_per'])

    # Married Couple Per
    zip_mrd_cpl_per = norm_mrd_cpl_per.norm_mrd_cpl_per(zip_data['mrd_cpl_per'])

    # Non Fam househoulds per
    zip_nonfam_hsehlds_per = norm_nonfam_hsehlds_per.norm_nonfam_hsehlds_per(zip_data['nonfam_hsehlds_per'])

    # Urban population percentage
    zip_urban_pop_per = norm_urban_pop_per.norm_urban_pop_per(zip_data['urban_pop_per'])

    # Urban inside pop percentage
    zip_urban_inside_area_pop_per = norm_urban_inside_area_pop_per.norm_urban_inside_area_pop_per(zip_data['urban_inside_area_pop_per'])

    # Rural pop per
    zip_rural_pop_per = norm_rural_pop_per.norm_rural_pop_per(zip_data['rural_pop_per'])

    # Urban Cluse pop per
    zip_urban_clsuer_per = norm_urban_cluster_pop_per.norm_urban_cluster_pop_per(zip_data['urban_cluster_pop_per'])

    # Overall Conversion
    overall_conversion = min(conversion_data['overall_conversion']/.05,1.)

    # Campaign Conversion
    campaign_conversion = min(conversion_data['campaign_conversion']/.05,1.)

    # Source Conversion
    source_conversion = min(conversion_data['source_conversion']/.05,1.)

    # State Conversion
    state_conversion = min(conversion_data['state_conversion']/.05,1.)

    # Zip Short Conversion
    zip_short_conversion = min(conversion_data['zip_short_conversion']/.05,1.)

    # Dictionary of normalized data
    form_data = [data['lead_id'],credit_high,credit_medium,credit_low,education_bachelors,education_high,education_masters,education_no_high,education_associates,education_some_college,gender_male,marital_divorced,marital_married,marital_single,no_prior,norm_age,norm_drivers,norm_vehicles,nstd_prior,pref_prior,prior_less_than_one,prior_more_than_two,prior_one_to_two,resident_other,resident_own,resident_rent,std_prior,use_business,use_pleasure,use_work,vehicle_age,campaign_conversion,source_conversion,state_conversion,zip_short_conversion, converted]
    fields = ['lead_id','credit_high','credit_medium','credit_low','education_bachelors','education_high','education_masters','education_no_high','education_associates','education_some_college','gender_male','marital_divorced','marital_married','marital_single','no_prior','norm_age','norm_drivers','norm_vehicles','nstd_prior','pref_prior','prior_less_than_one','prior_more_than_two','prior_one_to_two','resident_other','resident_own','resident_rent','std_prior','use_business','use_pleasure','use_work','vehicle_age','campaign_conversion','source_conversion','state_conversion','zip_short_conversion','converted']

    return fields, form_data

