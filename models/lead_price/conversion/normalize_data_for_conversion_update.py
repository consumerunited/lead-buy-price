# Function for getting data data ready to be fed to Conversion Model

# Goji normalizers
from models.normalizers.goji_normalizers import goji_norm_prior_policy_carrier
from models.normalizers.goji_normalizers import goji_norm_home_ownership
from models.normalizers.goji_normalizers import goji_norm_prior_policy_length
from models.normalizers.goji_normalizers import goji_norm_prior_policy_limit
from models.normalizers.goji_normalizers import goji_norm_primary_driver_gender
from models.normalizers.goji_normalizers import goji_norm_primary_driver_marital_status
from models.normalizers.goji_normalizers import goji_norm_primary_driver_age
from models.normalizers.goji_normalizers import goji_norm_primary_driver_education
from models.normalizers.goji_normalizers import goji_norm_primary_driver_credit
from models.normalizers.goji_normalizers import goji_norm_count_drivers
from models.normalizers.goji_normalizers import goji_norm_vehicle_age
from models.normalizers.goji_normalizers import goji_norm_vehicle_use
from models.normalizers.goji_normalizers import goji_norm_count_vehicles
from models.normalizers.goji_normalizers import goji_norm_email_provider
from models.normalizers.goji_normalizers import goji_norm_email_domain

# Zip Code Normalizers
from models.normalizers.zip_code_normalizers import norm_median_earning
from models.normalizers.zip_code_normalizers import norm_bach_or_higher
from models.normalizers.zip_code_normalizers import norm_total_pop
from models.normalizers.zip_code_normalizers import norm_cmtg_public_per
from models.normalizers.zip_code_normalizers import norm_rural_pop_per
from models.normalizers.zip_code_normalizers import norm_urban_pop_per
from models.normalizers.zip_code_normalizers import norm_mrd_cpl_per
from models.normalizers.zip_code_normalizers import norm_oo_value_median
from models.normalizers.zip_code_normalizers import norm_hsehld_utility_gas_per
from models.normalizers.zip_code_normalizers import norm_hsehld_electricity_per
from models.normalizers.zip_code_normalizers import norm_owner_occupied_per
from models.normalizers.zip_code_normalizers import norm_unit_median_rm
from models.normalizers.zip_code_normalizers import norm_per_capita_income

# Standard Normalizers
import datetime
from models.normalizers.standard_normalizers import norm_day_of_month
from models.normalizers.standard_normalizers import norm_hour
from models.normalizers.standard_normalizers import norm_minute
from models.normalizers.standard_normalizers import norm_weekday

# Jornaya Data
from models.normalizers.jornaya_normalizers import jornaya_norm_age
from models.normalizers.jornaya_normalizers import jornaya_norm_duration
from models.normalizers.jornaya_normalizers import jornaya_norm_entities
from models.normalizers.jornaya_normalizers import jornaya_norm_hops

def normalize_data_for_conversion(data, zip_data, conversion_data, jornaya_data, train_flag = 1):

    
    # Normalize Prior Carrier
    missing_prior, no_prior, pref_prior, std_prior, nstd_prior, unsure_prior = goji_norm_prior_policy_carrier.goji_norm_prior_policy_carrier(data['prior_carrier'])

    # Normalize Resident Status
    resident_own, resident_rent, resident_other, resident_unknown = goji_norm_home_ownership.goji_norm_home_ownership(data['homeownership'])

    # Normalize Current Coverage Length
    prior_none, prior_less_than_one, prior_one_to_two, prior_more_than_two = goji_norm_prior_policy_length.goji_norm_prior_policy_length(data['months_with_prior_carrier'])

    # Normalize Current Liability Limit
    limit_none, limit_high, limit_low = goji_norm_prior_policy_limit.goji_norm_prior_policy_limit(data['liability_limit'])

    # Driver Gender
    gender_male, gender_female, gender_other = goji_norm_primary_driver_gender.goji_norm_primary_driver_gender(data['gender'])

    # Driver Marital Status
    marital_divorced, marital_married, marital_other, marital_single, marital_widowed = goji_norm_primary_driver_marital_status.goji_norm_primary_driver_marital_status(data['marital_status'])

    # Normalize Driver Ages
    norm_age = goji_norm_primary_driver_age.goji_norm_primary_driver_age(data['age'])

    # Normalize Driver Education
    education_associates, education_bachelors, education_high, education_masters, education_no_high, education_phd, education_some_college, education_other = goji_norm_primary_driver_education.goji_norm_primary_driver_education(data['education'])

    # Normalize Driver Credit
    credit_high, credit_medium, credit_low, credit_none = goji_norm_primary_driver_credit.goji_norm_primary_driver_credit(data['credit'])

    # Normalize Number of Drivers
    norm_drivers = goji_norm_count_drivers.goji_norm_count_drivers(data['drivers'])

    # Normalize Age of Vehicle
    vehicle_age = goji_norm_vehicle_age.goji_norm_vehicle_age(data['vehicle_age'])

    # Normalize vehicle use
    use_other, use_business, use_pleasure, use_work = goji_norm_vehicle_use.goji_norm_vehicle_use(data['vehicle_use'])

    # Normalize number of vehicles
    norm_vehicles = goji_norm_count_vehicles.goji_norm_count_vehicles(data['vehicles'])

    # Total LTV
    if train_flag == 1:
        converted = data['converted']
    else:
        converted = None

    # Median Earnings
    zip_median_earning = norm_median_earning.norm_median_earning(zip_data['median_earning'])
    zip_bach_or_higher = norm_bach_or_higher.norm_bach_or_higher(zip_data['overall_bach_or_hgher'])
    zip_total_pop = norm_total_pop.norm_total_pop(zip_data['total_pop'])
    zip_cmtg_public_per = norm_cmtg_public_per.norm_cmtg_public_per(zip_data['cmtg_public_per'])
    zip_rural_pop_per = norm_rural_pop_per.norm_rural_pop_per(zip_data['rural_pop_per'])
    zip_urban_pop_per = norm_urban_pop_per.norm_urban_pop_per(zip_data['urban_pop_per'])
    zip_mrd_cpl_per = norm_mrd_cpl_per.norm_mrd_cpl_per(zip_data['mrd_cpl_per'])
    zip_oo_value_median = norm_oo_value_median.norm_oo_value_median(zip_data['oo_value_median'])
    zip_hsehld_utility_gas_per = norm_hsehld_utility_gas_per.norm_hsehld_utility_gas_per(zip_data['hsehld_utility_gas_per'])
    zip_owner_occupied_per = norm_owner_occupied_per.norm_owner_occupied_per(zip_data['owner_occupied_per'])
    zip_unit_median_rm = norm_unit_median_rm.norm_unit_median_rm(zip_data['unit_median_rm'])
    zip_per_capita_income = norm_per_capita_income.norm_per_capita_income(zip_data['per_capita_income'])

    # Date and timing stuff
    if train_flag == 1:
        date = data['acquisition_date']
    else:
        date = datetime.datetime.now()

    day_of_month = norm_day_of_month.norm_day_of_month(date)
    hour = norm_hour.norm_hour(date)
    minute = norm_minute.norm_minute(date)
    weekday_mon, weekday_tue, weekday_wed, weekday_thur, weekday_fri, weekday_sat, weekday_sun = norm_weekday.norm_weekday(date)

    # Jornaya Data
    jornaya_age = jornaya_norm_age.jornaya_norm_age(jornaya_data['jornaya_age'])
    jornaya_duration = jornaya_norm_duration.jornaya_norm_duration(jornaya_data['duration'])
    jornaya_entities = jornaya_norm_entities.jornaya_norm_entities(jornaya_data['total_entities'])
    jornaya_hops = jornaya_norm_hops.jornaya_norm_hops(jornaya_data['total_hops'])

    # Historical Conversion Metrics
    overall_conversion = min(conversion_data['overall_conversion']/.05,1.)
    campaign_conversion = min(conversion_data['campaign_conversion']/.05,1.)
    source_conversion = min(conversion_data['source_conversion']/.05,1.)
    state_conversion = min(conversion_data['state_conversion']/.05,1.)
    zip_short_conversion = min(conversion_data['zip_short_conversion']/.05,1.)

    # Dictionary of normalized data
    form_data = [data['lead_id'],credit_high,credit_medium,credit_low,education_bachelors,education_high,education_masters,education_no_high,education_associates,education_some_college,gender_male,marital_divorced,marital_married,marital_single,no_prior,norm_age,norm_drivers,norm_vehicles,nstd_prior,pref_prior,prior_less_than_one,prior_more_than_two,prior_one_to_two,resident_other,resident_own,resident_rent,std_prior,use_business,use_pleasure,use_work,vehicle_age,campaign_conversion,source_conversion,state_conversion,zip_short_conversion,zip_median_earning,zip_bach_or_higher,zip_total_pop,zip_cmtg_public_per,zip_rural_pop_per,zip_urban_pop_per,zip_mrd_cpl_per,zip_oo_value_median,zip_hsehld_utility_gas_per,zip_owner_occupied_per,zip_unit_median_rm,zip_per_capita_income,    day_of_month, hour, minute, weekday_mon, weekday_tue, weekday_wed, weekday_thur, weekday_fri, weekday_sat, weekday_sun, jornaya_age,jornaya_duration,jornaya_entities,jornaya_hops, converted]
    fields = ['lead_id','credit_high','credit_medium','credit_low','education_bachelors','education_high','education_masters','education_no_high','education_associates','education_some_college','gender_male','marital_divorced','marital_married','marital_single','no_prior','norm_age','norm_drivers','norm_vehicles','nstd_prior','pref_prior','prior_less_than_one','prior_more_than_two','prior_one_to_two','resident_other','resident_own','resident_rent','std_prior','use_business','use_pleasure','use_work','vehicle_age','campaign_conversion','source_conversion','state_conversion','zip_short_conversion', 'zip_median_earning','zip_bach_or_higher','zip_total_pop','zip_cmtg_public_per','zip_rural_pop_per','zip_urban_pop_per','zip_mrd_cpl_per','zip_oo_value_median','zip_hsehld_utility_gas_per','zip_owner_occupied_per','zip_unit_median_rm','zip_per_capita_income','day_of_month', 'hour', 'minute', 'weekday_mon', 'weekday_tue', 'weekday_wed', 'weekday_thur', 'weekday_fri', 'weekday_sat', 'weekday_sun','jornaya_age','jornaya_duration','jornaya_entities','jornaya_hops','converted']

    return fields, form_data

