import random
import numpy as np
from cache.cache import cache
import datetime

# Database Buisness
from db_model.db import db
from db_model.model import model
from db_model.model_parameters import model_parameters

model_version = 2

@cache.memoize(timeout=300)
def get_model_params():
	model_id = db.session.query(model).filter(model.name == 'price').first()
	parameter_dict = {}
	for parameter in db.session.query(model_parameters).filter((model_parameters.model_id == model_id.model_id)).all():
		parameter_dict[parameter.name] = parameter.value

	return parameter_dict

def ltv_bands(ltv):
    if ltv < 750:
        ltv_band = 'LOW'
    elif ltv < 1300:
        ltv_band = 'MEDIUM'
    else:
        ltv_band = 'HIGH'
    return ltv_band

def conversion_bands(conversion):
    if conversion < .0096:
        conversion_band = 'LOW'
    elif conversion < 0.01:
        conversion_band = 'MEDIUM'
    else:
        conversion_band = 'HIGH'
    return conversion_band

def generate_price(ltv, conv, market, demand, campaign_id = None, vehicles = None):
	
	parameter_dict = get_model_params()

	# Generate bands of outcomes to adjust bidding
	ltv_band = ltv_bands(ltv)
	conversion_band = conversion_bands(conv)

	# margin = float(parameter_dict["MARGIN" + "_" + ltv_band + "_" + conversion_band])
	margin = float(parameter_dict['margin'])
	min_ltv = float(parameter_dict['min_ltv'])

	# Adjust based on hour or weekend
	if (datetime.datetime.now().hour >= 18 or datetime.datetime.now().hour <= 7):
		margin = margin +  1
	elif datetime.datetime.today().weekday() == 5:
		margin = margin + 1
	elif datetime.datetime.today().weekday() == 6:
		margin = margin + 1
        
        if campaign_id == 81:
		margin = margin + 1.5
	elif campaign_id == 84:
		margin = margin + 1.5
    #elif campaign_id == 269:
    #        margin = margin - 1
	base_price = (ltv*conv/margin) * demand * market

	
	# Constrain price between $0 and $10
	bid_price = max(min(base_price,10.0),0.0)
	if ltv < min_ltv:
		bid_price = 0

	    # Temporary Adjustment for UE Search Leads
	if campaign_id == 251:
		bid_price = 8.00

    # Temporary Adjustment for Union Square Media to Bid 0
	elif campaign_id == 252:
		bid_price = min(bid_price,5.00)

    # Temporary Adjustment for AWL Multicar set bid to 2.75
	elif campaign_id == 244:
		#print vehicles
		if vehicles == 1:
			bid_price = min(bid_price,2.00)
		else:
			bid_price = min(bid_price,5.00)

    # Temporary Adjustment for LMB set bid to 2.75
	elif campaign_id == 180:
		bid_price = min(bid_price,3.25)
	elif campaign_id == 260:
		bid_price = min(bid_price,6.00)

	# Adjustment for UE Aged Campaign
	elif campaign_id == 257:
		bid_price = 1


	return base_price, bid_price, model_version, margin
