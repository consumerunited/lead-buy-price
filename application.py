import logging.config

from flask import Flask, Blueprint
from flask_sqlalchemy import SQLAlchemy
from analytics.get_price.get_price import ns as lead_price_namespace
from analytics.model_data.model_data import ns as model_data_namespace
from analytics.lead_score.lead_score import ns as lead_score_namespace
from analytics.duplicate_detection.detect_duplicate import ns as duplicate_namespace
from analytics.customer_recommendation.customer_recommendation import ns as customer_recommendation

from analytics.restplus import analytics
from db_model.db import db
from cache.cache import cache
import os

# Configure App
app = Flask(__name__)
logging.config.fileConfig(os.environ['APP_ROOT'] + '/configuration/logging.conf')
app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)
#print db.metadata.tables.keys()
cache.init_app(app)

log = logging.getLogger(__name__)

# Initialize and start app
def initialize_app(flask_app):

    blueprint = Blueprint('analytics', __name__, url_prefix='/lead_price')
    analytics.init_app(blueprint)
    analytics.add_namespace(lead_price_namespace)
    analytics.add_namespace(model_data_namespace)
    analytics.add_namespace(lead_score_namespace)
    analytics.add_namespace(duplicate_namespace)
    analytics.add_namespace(customer_recommendation)
    flask_app.register_blueprint(blueprint)


def main():
    initialize_app(app)
    #log.info('>>>>> Starting development server at http://{}/ <<<<<'.format(app.config['SERVER_NAME']))
    app.run()

if __name__ == "__main__":
    main()
