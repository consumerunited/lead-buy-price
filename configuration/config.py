import os
basedir = os.path.abspath(os.path.dirname(__file__) )


class Config(object):
    #Flassk settings
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = '\xccI\x12s\xef\xcb\xf8\x04JWDY\xf2"\xb0\xe2l6\xb3\xcb\x9ca\xb7\xb5\xd5\x06\xdf\xe1U\xe1'
    #SERVER_NAME = '0.0.0.0:8080'

    # Flask-Restplus settings
    SWAGGER_UI_DOC_EXPANSION = 'list'
    RESTPLUS_VALIDATE = True
    RESTPLUS_MASK_SWAGGER = False
    ERROR_404_HELP = False

    # Database Stuff
    #SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

    # Arango Stuff
    ARANGO_URL = "http://10.0.1.128:8529"
    ARANGO_USER = 'dup_database_prod'
    ARANGO_PASSWORD = 'MLuMaoPUxSStuLCiWBnq'
    ARANGO_DB_NAME = 'prod_customer_db'
    
class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = "mysql://goji_analytics:goji_analytics@internal-db.cpzuqtj4hq4e.us-east-1.rds.amazonaws.com:3306/leadbuy_analytics"


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "mysql://goji_analytics:goji_analytics@goji-dw-stage.cpzuqtj4hq4e.us-east-1.rds.amazonaws.com:3306/leadbuy_analytics"


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "mysql://goji_analytics:goji_analytics@goji-dw-dev.cpzuqtj4hq4e.us-east-1.rds.amazonaws.com:3306/leadbuy_analytics"
    
    #SQLALCHEMY_DATABASE_URI="mysql://analytics_leadbuy:password@localhost:3306/analytics_leadbuy"
