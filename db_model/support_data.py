# Table for storing support data for models, parent model

from db import db
from sqlalchemy_continuum import make_versioned

# We want to track changes to this table
make_versioned(user_cls=None)

# Table for holding parameters
class support_data(db.Model):
    # Name of the table
    __tablename__ = 'support_data'
    __versioned__ = {}
    
    # Columns
    support_data_id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(50), nullable = False)
    description = db.Column(db.String(100), nullable = False)

    # Unique Constraint for parameter name
    __table_args__ = (db.UniqueConstraint(support_data_id,name, name='support_data_name_uc'),)
    
    # Function for getting data to db
    def __init__(self, name, description):
        self.name = name
        self.description = description

db.configure_mappers()
