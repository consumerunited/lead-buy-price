# Table for storing support data for models, parent model

from db import db
from sqlalchemy_continuum import make_versioned

# We want to track changes to this table
make_versioned(user_cls=None)

# Table for holding parameters
class support_data_values(db.Model):
    # Name of the table
    __tablename__ = 'support_data_values'
    __versioned__ = {}
    
    # Columns
    support_data_id = db.Column(db.Integer,db.ForeignKey('support_data.support_data_id'))
    support_data_value_id = db.Column(db.Integer, primary_key = True)
    key = db.Column(db.String(250), primary_key = False)
    value = db.Column(db.Float(), nullable = False)

    # Unique Constraint for parameter name
    __table_args__ = (db.UniqueConstraint(support_data_id, key, name='support_data_key_name_uc'),)
    
    # Function for getting data to db
    def __init__(self, support_data_id, key, value):
        self.support_data_id = support_data_id
        self.key = key
        self.value = value

db.configure_mappers()
