# Table for storing parameters for models, parent model

from db import db
from sqlalchemy_continuum import make_versioned

# We want to track changes to this table
make_versioned(user_cls=None)

# Table for holding parameters
class model(db.Model):
    # Name of the table
    __tablename__ = 'model'
    __versioned__ = {}
    
    # Columns
    model_id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(50), nullable = False)
    description = db.Column(db.String(5000))

    # Unique Constraint for parameter name
    __table_args__ = (db.UniqueConstraint(name, name='model_name_uc'),)
    
    # Function for getting data to db
    def __init__(self, name, description):
        self.name = name
        self.description = description


db.configure_mappers()
