Lead Buy Price
====================

### What is this repository for? ###

* This project is intended to, given lead information that confirms to the Goji Standard return a price based on:
	* Goji's perceived value of a lead (i.e. LTV)
	* Goji's ability to convert a lead 
	* Goji's demand for a lead
	* The broader market conditions
* Each of these models will exist separately from one another and be ultimately combined to create a single price
* The model will be exposed to Goji's existing service via a light weight API that will allow users to post a lead and will return:
	* Price
	* LTV Factors
	* Converions Factors
	* Demand Factors
	* Market Factors
	* Model versions
	* The exact layout of the return message is TBD and will confirmed with development
* These outputs will be stored to allow us to judge the performance of the model over time
* Version 0.0

### How do I get set up? ###

* Clone repository
* Create virtual environment in repo 
	* virtualenv env
* Start the virtual environment
	* source env/bin/activate
* Install requirements
	* pip install -r requirements.txt
	* Install redis (used for caching database results in memory) and make sure its running
* If this is the first time connecting to a db run or there are updates to the db:
	* python manage.py db upgrade
* Test that the app starts
	* python application.py
* Environment variables control "where" the app is running. The code includes a .env file that defaults to the 'Development' mode. Executing the code below will 1) start the virtual environment when you cd into the directory and 2) set system variables. Make sure the virtual environment is not activated when executing below and autoenv is install (pip install autoenv):
	* echo "source 'which activate.sh'" >> ~/.bashrc
	* source ~/.bashrc

### Who do I talk to? ###

* Analytics
* Garrett Biemer (garrett.biemer@goji.com)
