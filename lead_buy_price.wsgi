import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('/var/www/lead-buy-price/env/bin/activate_this.py')

# Add the app's directory to the PYTHONPATH
#sys.path.append('/home/django_projects/MyProject')
sys.path.append('/var/www/lead-buy-price/')

os.environ['APP_SETTINGS'] = 'configuration.config.ProductionConfig'
os.environ['APP_ROOT'] = '/var/www/lead-buy-price'
# Activate your virtual env
activate_env=os.path.expanduser("/var/www/lead-buy-price/env/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))

# Set up app
from application import app as application, initialize_app

initialize_app(application)

if __name__ == '__main__':
    application.run()

